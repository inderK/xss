#!/bin/bash
# 开发新功能
message1='
【修改类型】：开发新功能 
【任务描述】：
【任务1D】：
【需求ID】：
【修改描述】：
【影响范国】：
【责任人】：
【检视人】：
'
# 修改Bug
message2='
【修改类型】：修改Bug
【Bug描述】：
【在线填】:
【BugID】：
【问题原因】：
【修改描述】：
【影响范国】：
【责任人】：
【检视人】：
'
# echo $message2 $message1

timestamp=$(date +%Y-%m-%d-%H:%M:%S-%A)
message='（脚本自动提交）'
read -p "请输入提交备注 : " something
# 判断如果不是空的话
if [ ! -z "$something" ];
then
# 重新赋值必须这么做，否则直接破坏if结构
export message="（$something）"
fi
echo "提交时间为: $timestamp$message"
git add .
git commit -m "提交时间为: $timestamp ($message)"
git pull origin master
git push origin master
echo 完成提交，按任意键离开...
read
# 请注意，bash shell脚本写在package里面的话会有很多问题，最好的解决方案是使用钩子函数来格式化提交，而这里只需要使用便捷的脚本提交即可，所以这样直接