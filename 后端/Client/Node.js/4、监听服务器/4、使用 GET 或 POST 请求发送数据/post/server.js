var http = require('http');
var fs = require('fs');
var url = require('url');
var querystring = require('querystring');
 
function startServer(route, handle) {
    var onRequest = function (request, response) {
 
        var pathname = url.parse(request.url).pathname;
        console.log('Request received ' + pathname);
 
        var data = "";//定义一个空字符串
        request.on("error", function (err) {//当错误的时候，输出错误信息
            console.error(err);
        }).on("data", function (chunk) {//接收数据的时候，把数据组合在一起
            data += chunk;
        }).on('end', function () {//接收完数据以后，把数据传给后端的处理函数
            if (request.method === "POST") {//post请求的时候
                //直接传入data，取到的是一个字符串
                //querystring.parse(data) 把字符串解析成json对象。
                route(handle, pathname, response, querystring.parse(data));
            } else {//get请求的时候
                var params = url.parse(request.url, true).query;
                route(handle, pathname, response, params);
            }
        });
    }
 
    var server = http.createServer(onRequest);
 
    server.listen(3000, '127.0.0.1');
    console.log('Server started on localhost port 3000');
}
 
module.exports.startServer = startServer;