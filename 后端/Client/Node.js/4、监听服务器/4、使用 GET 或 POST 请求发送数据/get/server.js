var http = require('http');
var fs = require('fs');
var url = require('url');//url库，对url进行一些处理，可以取一些主机名、协议之类的
 
function startServer(route, handle) {
    var onRequest = function (request, response) {
        // console.log('Request received ' + request.url);
 
        // http://localhost:3000/api/v1/records?a=123
        // 取地址问号前面、端口号后面的路径。即 /api/v1/records
        var pathname = url.parse(request.url).pathname;
        console.log('Request received ' + pathname);
 
        //解析地址端口号后面的数据，
        //第一个参数：地址问号前面的数据
        //第二个参数：地址问号后面的数据，true输出json对象，false输出字符串
        var params = url.parse(request.url, true).query; //{"a":"123"}
        //var params = url.parse(request.url, false).query;//"a=123" 
 
        route(handle, pathname, response, params);
 
    }
 
    var server = http.createServer(onRequest);
 
    server.listen(3000, '127.0.0.1');
    console.log('Server started on localhost port 3000');
}
 
module.exports.startServer = startServer;