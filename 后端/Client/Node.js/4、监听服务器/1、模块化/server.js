var http = require('http');
var fs = require('fs');
 
//把启动server、响应客户端的代码，放在一个函数里面
function startServer() {
    var onRequest = function (request, response) {
        console.log('Request received');
        response.writeHead(200, { 'Content-Type': 'text/html' });
        var myReadStream = fs.createReadStream(__dirname + '/index.html', 'utf8');
        myReadStream.pipe(response);
    }
 
    var server = http.createServer(onRequest);
 
    server.listen(3000, '127.0.0.1');
    console.log('Server started on localhost port 3000');
}
 
// 暴露startServer函数
//下面的代码，等同于 module.exports.startServer = startServer;
exports.startServer = startServer;