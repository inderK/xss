var http = require('http');
 
var onRequest = function (request, response) {
    console.log('Request received');
    // response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.writeHead(200, { 'Content-Type': 'application/json' });
 
    //定义json对象
    var myObj = {
        name: "yyh",
        job: "android",
        age: 18
    };
    
    //response.end('hello from server');
    //把json数据作为响应内容输出出来
    response.end(JSON.stringify(myObj));
}
 
var server = http.createServer(onRequest);
 
server.listen(3000, '127.0.0.1');
console.log('server started on localhost port 3000');
// stringify方法使myObj变成一个字符串类型的json，它其实是一个序列化的过程，这样就可以用于传输。如果你要在客户端取出来，那么就要进行反序列化！