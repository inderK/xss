function Example1() {
    var http = require('http');//引入http库

    //创建服务器
    var server = http.createServer(function (request, response) {
        console.log('request received');
        //1、状态码  2、响应返回的内容类型
        response.writeHead(200, { 'Content-type': 'text/plain' });

        //response.write('hello from server');
        //response.end();

        //这样写也可以，同上面两句
        response.end('hello from server');

        //另外 response.write 是可以写多个的,如下：
        // response.write('<html>');
        // response.write('<body>');
        // response.write('<h1>Hello, World!</h1>');
        // response.write('</body>');
        // response.write('</html>');
        // response.end();

    });

    //1、监听端口号  2、ip地址（这里是本地地址）
    server.listen(3000, '127.0.0.1');
    console.log('server started on localhost port 3000');
}

function Example2() {
    var http = require('http');

    var onRequest = function (request, response) {
        console.log('Request received');
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('hello from server');
    }

    var server = http.createServer(onRequest);

    server.listen(3000, '127.0.0.1');
    console.log('server started on localhost port 3000');
}

// Example1()
Example2()