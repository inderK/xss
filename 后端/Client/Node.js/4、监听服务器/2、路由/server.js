var http = require('http');
var fs = require('fs');

//把启动server、响应客户端的代码，放在一个函数里面
function startServer() {
    var onRequest = function (request, response) {

        console.log('Request received ' + request.url);

        if (request.url === '/' || request.url === '/home') {
            //其实是对这些地址的判断：http://localhost:3000/、 http://localhost:3000/home
            // 然后显示 index.html 页面
            response.writeHead(200, { 'Content-Type': 'text/html' });
            fs.createReadStream(__dirname + '/index.html', 'utf8').pipe(response);

        } else if (request.url === '/review') {
            //其实是对这个地址的判断：http://localhost:3000/review
            // 然后显示 review.html 页面
            response.writeHead(200, { 'Content-Type': 'text/html' });
            fs.createReadStream(__dirname + '/review.html', 'utf8').pipe(response);
        } else if (request.url === '/api/v1/records') {
            //其实是对这个地址的判断：http://localhost:3000/api/v1/records
            // 然后显示 json数据
            response.writeHead(200, { 'Content-Type': 'application/json' });
            var jsonObj = {
                name: "yyh"
            };
            response.end(JSON.stringify(jsonObj));
        } else {//其它路由就显示 404.html页面
            response.writeHead(404, { 'Content-Type': 'text/html' });
            fs.createReadStream(__dirname + '/404.html', 'utf8').pipe(response);
        }
    }

    var server = http.createServer(onRequest);

    server.listen(3000, '127.0.0.1');
    console.log('Server started on localhost port 3000');
}

// 暴露startServer函数
//下面的代码，等同于 module.exports.startServer = startServer;
exports.startServer = startServer;