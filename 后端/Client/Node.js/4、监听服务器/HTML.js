function Example1() {
    var http = require('http');

    var onRequest = function (request, response) {
        console.log('Request received');
        // response.writeHead(200, { 'Content-Type': 'text/plain' });//响应纯文本 
        // response.writeHead(200, { 'Content-Type': 'application/json' });//响应json
        response.writeHead(200, { 'Content-Type': 'text/html' });//响应html文件

        //定义字符串拼接的 html文件
        var htmlFile = '<!DOCTYPE html>' +
            '<html lang="en">' +

            '<head>' +
            '<meta charset="UTF-8">' +
            '<meta name="viewport" content="width=device-width, initial-scale=1.0">' +
            '<meta http-equiv="X-UA-Compatible" content="ie=edge">' +
            '<title>这是html页面</title>' +
            '</head>' +

            '<body>' +
            'hello wolrd' +
            '</body>' +

            '</html>'

        //把HTML数据作为响应内容输出出来
        response.end(htmlFile);
    }

    var server = http.createServer(onRequest);

    server.listen(3000, '127.0.0.1');
    console.log('server started on localhost port 3000');
}
function Example2() {

    var http = require('http');
    var fs = require('fs');

    var onRequest = function (request, response) {
        console.log('Request received');

        // response.writeHead(200, { 'Content-Type': 'text/html' });//响应html文件
        response.writeHead(200, { 'Content-Type': 'text/plain' });//响应纯文本

        var myReadStream = fs.createReadStream(__dirname + '/index.html', 'utf-8');

        //把HTML数据作为响应内容输出出来
        myReadStream.pipe(response);

    }

    var server = http.createServer(onRequest);

    server.listen(3000, '127.0.0.1');
    console.log('server started on localhost port 3000');
}

// Example1()
Example2()