function Example1() {
    var fs = require('fs');

    //创建读的流，它是输入流，读取一个文件把它放入流中，
    //__dirname我们在介绍全局对象的时候有介绍
    var myReadStream = fs.createReadStream(__dirname + '/readMe.txt', 'utf-8');
    // 我们会发现输出很多Buffer，所以说它是分段处理的，就是把文件截成一个个Buffer来处理的！那么到底怎么把文件读取出来呢？我们需要加一个编码格式

    //上面我们有介绍，这个流它是事件的一个实例，所以它有事件的一些特性
    //比如绑定一些监听函数之类的。
    myReadStream.on('data', function (chunk) {
        console.log('new chunk received');
        console.log(chunk);
    });
}

// 流读取文件-createReadStream
function Example2() {
    var fs = require('fs');
    var myReadStream = fs.createReadStream(__dirname + '/readMe.txt');
    myReadStream.setEncoding('utf-8');
    var data = "";
    myReadStream.on('data', function (chunk) { data += chunk; });
    //它还有一个监听函数，上面'data'是接收数据时候的监听，
    //'end'为接收完数据时候的监听，回调函数
    myReadStream.on('end', function () { console.log(data); })
}
// Example1()
Example2()

// 流写入文件-createWriteStream
function Example3() {
    var fs = require('fs');
    // 文件路径 '/writeMe.txt' 前面的斜杠千万不要忘了！花了我几分钟找bug
    var myWriteStream = fs.createWriteStream(__dirname + '/writeMe.txt');
    var writeData = "hello world";
    //写入数据。参数utf-8可以去掉
    myWriteStream.write(writeData, 'utf-8');
    //写入结束
    myWriteStream.end();
    //写入完成，回调方法
    myWriteStream.on('finish', function () {
        console.log('写入已经完成，finished！');
    });
}
Example3()