// 使用管道的话，代码量更少，一行就够了。myReadStream是读取的流，myWriteStream是写入的流
function Example1() {
    var fs = require('fs');

    var myReadStream = fs.createReadStream(__dirname + '/readMe.txt');
    var myWriteStream = fs.createWriteStream(__dirname + '/writeMe.txt');

    myReadStream.pipe(myWriteStream);

}

// Example1()


// 读取文件加密压缩打包
function Example2() {
    // 压缩
    var crypto = require('crypto');//加密库
    var fs = require('fs');
    var zlib = require('zlib');//压缩库

    //(node:32751) [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues. 
    //Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.
    //意思：由于安全性和可用性问题，buffer（）已弃用。请改用buffer.alloc（）、buffer.allocunsafe（）或buffer.from（）方法。
    // var password = new Buffer(process.env.PASS || 'password');

    var password = new Buffer.from(process.env.PASS || 'password');
    //参考博客：https://blog.csdn.net/m0_37263637/article/details/83501928

    var encryptStream = crypto.createCipher('aes-256-cbc', password);
    // 使用crypto.createCipher()和crypto.createDecipher() 应该避免使用，因为它们使用弱密钥派生函数（无盐的MD5）和静态初始化向量。建议使用crypto.pbkdf2()或crypto.scrypt()和导出密钥，并分别使用crypto.createCipheriv()和crypto.createDecipheriv()获取Cipher和Decipher对象。
    var gzip = zlib.createGzip();
    var readStream = fs.createReadStream(__dirname + "/readMe.txt"); // current file
    var writeStream = fs.createWriteStream(__dirname + '/out.gz');

    readStream // reads current file
        .pipe(encryptStream) // encrypts
        .pipe(gzip) // compresses
        .pipe(writeStream) // writes to out file
        .on('finish', function () { // all done
            console.log('done');
        });
}
// Example2()

// 读取文件解压解密输出到终端
function Example3() {
    // 解压
    var crypto = require('crypto');
    var fs = require('fs');
    var zlib = require('zlib');

    //var password = new Buffer(process.env.PASS || 'password');
    var password = new Buffer.from(process.env.PASS || 'password');
    var decryptStream = crypto.createDecipher('aes-256-cbc', password);

    var gzip = zlib.createGunzip();
    var readStream = fs.createReadStream(__dirname + '/out.gz');

    readStream // reads current file
        .pipe(gzip) // uncompresses
        .pipe(decryptStream) // decrypts
        .pipe(process.stdout) // writes to terminal
        .on('finish', function () { // finished
            console.log('done');
        });
}
Example3()