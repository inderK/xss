 
var events = require('events');
//这个库是一个工具库，里面包含了很多工具，它也是nodejs的核心库之一
//我们可参考api：https://nodejs.org/dist/latest-v9.x/docs/api/util.html
var util = require('util');
 
// 定义一个Person类，这是javaScript的写法
var Person = function(name) {
    this.name = name;
}
 
//然后我们调用工具库里面的函数inherits
//让 Person类 继承 events.EventEmitter 事件类
util.inherits(Person, events.EventEmitter);
 
//新建三个Person对象
var xiaoming = new Person('xiaoming');
var xiaohua = new Person('xiaohua');
var xiaobai = new Person('xiaobai');
 
// 把三个Person对象 放到 数组中
var person = [xiaoming,xiaohua,xiaobai];
 
// forEach 循环 person数组，
// 为每个Person对象，依次绑定一个监听的函数on
person.forEach(function(person) {
    person.on('speak',function(message) {
        console.log(person.name + "说：" + message);
    })
});
 
//手动用代码来触发
xiaoming.emit('speak', '123');
xiaohua.emit('speak', '456');
xiaobai.emit('speak', '789');