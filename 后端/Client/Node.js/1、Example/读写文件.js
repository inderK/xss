function read() {
    //导入nodejs文件系统的一个库
    var fs = require('fs');
    //参数1：文件名路径，路径需要加双引号。
    //因为是当前目录，所以直接可以写文件名  
    //参数2：编码格式
    var readMe = fs.readFileSync("readMe.txt", "utf-8");
    console.log(readMe)
}
function write() {
    var fs = require('fs');
    var readMe = fs.readFileSync("readMe.txt", "utf-8");
    //参数1：要写入的文件名路径。没有的话，它会自动新建
    //参数2：要写入的内容。
    fs.writeFileSync("writeMe.txt", readMe);
    //fs.writeFileSync("writeMe.txt", "190910马老师-怒放的生命");
}

read()
// write()

// nodejs执行JavaScript的时候它是单线程的，这里要注意的是：我并没有说nodejs是单线程的。
// 这种I/O操作，比如说像数据库连接是很常见的，也是最耗时的。那么怎么来解决这个问题呢？这个时候我们可以用到异步
// nodejs它维护了一个事件队列，上面三行语句中，第一句还是会先执行，执行第二句的时候它就在事件队列当中注册了一个事件，告诉这个事件队列，我将要去读取一个文件。
// 但是里面的回调函数并没有被马上执行，这个操作是瞬间完成的，完成之后它就会执行主线程的第三条语句 console.log("finished");  
// 当主线程空闲的时候，它就会去找事件队列里面的事件，把它取出来。在此同时发起一个线程去执行事件队列里面的事件，在这里是读取一个文件，当读完成功以后，它再告诉主线程我已经执行成功。
function readSync() {
    var fs = require('fs');
    var readMe = fs.readFile("readMe.txt", "utf-8", function (err, data) { console.log(data); });
    // 延迟5秒执行。我们可以用这个模拟感受下，阻塞的状态！
    //也就是说，有如下耗时的同步操作，那就会阻塞整个线程的运行，
    //就算是上面异步的方法，也会被阻塞。当遇见这种情况，这时候你写nodejs，
    //都是要用异步的方法来处理
    //var waitTill = new Date(new Date().getTime() + 5 * 1000);
    //while (waitTill > new Date()) {}
    console.log("finished", readMe); //这里的readMe就未定义了
}

function writeSync() {
    var fs = require('fs');
    // var readMe = fs.readFile("readMe.txt","utf-8",function(err, data) {
    //     console.log(data);
    // });
    var data = "写写写";
    fs.writeFile("writeMe.txt", data, function () { console.log("写入完毕！"); });
    console.log("finished");
}

// readSync()
writeSync()