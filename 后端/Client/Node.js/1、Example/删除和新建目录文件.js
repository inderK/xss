function deletefile() {
    var fs = require('fs');
    //unlink也是异步方法，异步方法一般都有回调函数
    fs.unlink("writeMe.txt", function (params) { console.log("delete writeMe.txt file"); });
    //上面是异步的方法，如果是同步的方法后面加Sync
    //参数直接写文件名，也不用加回调函数，这里就不演示了！
    //fs.unlinkSync
}
// 创建、删除目录
// 另外需要注意的是创建一般是 “mk” 前缀开头，删除一般以 “rm” 前缀为开头
function writemenu() {
    var fs = require('fs');

    //异步方法：
    //当前目录创建目录
    //fs.mkdirSync('temp');
    //删除指定目录temp
    //fs.rmdirSync('temp');

    //同步方法：
    //创建目录temp，同时把 readMe.txt 文件的内容，
    //写入到 ./temp/writeMe.txt 文件中
    //注：temp目录下没有 writeMe.txt 文件，写入内容的时候它会自动创建 
    fs.mkdir('temp', function () {
        fs.readFile('readMe.txt', 'utf-8', function (err, data) {
            fs.writeFile('./temp/writeMe.txt', data, function () {
                console.log('拷贝成功！！！');
            });
        });
    });
}

// deletefile()
writemenu()
