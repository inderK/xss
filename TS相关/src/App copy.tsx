import React from 'react';
import ZJ from "./zj"
import './App.css';
// import logo from './logo.svg';
// 接口类型不需要导入，全局都有效
// import Person from "./data.d.ts"


// tsx既包含类型信息又可执行代码
//在运行时不会保留任何类型信息——没有任何信息表明某个变量 x 被声明为SomeType 类型。
//也就是tsc App.tsx的话，是不会有类型声明的
// 鼠标放在 forEach 上查看类型
// strs.forEach


//一个简单的工具函数
type CallsFunction = (callback: (result: string) => any) => void;
const fun: (string: string, cb: CallsFunction, callback?: (result: string) => any,) => number = (str, cb, callback) => {
  cb((e) => { console.log(e) })
  callback && callback(str) && console.log(callback(str))
  return 222
}
//void 是指定返回类型为无类型，一般会用在没有返回值的函数上
function funn(): void {
  console.log("1222")
  // return "11"
}
console.log(fun("111", (e) => { e("999") }, (e) => { return e }), funn())

//可以使用函数接口来定义一个函数，规范它的入参和返回，但是接口定义只是定义，而不必让函数入参也和接口定义的名字一样
let mySearch: SearchFunc;
mySearch = function (source1: string, subString2: string) {
  let result = source1.search(subString2);
  return result > -1;
}

//使用泛型 泛型在玄同项目中并没有怎么使用而它是非常重要的
// 我们需要一种方法使返回值的类型与传入参数的类型是相同的
function identity(arg: { arg: any }, num: number): { arg: { arg: any }, num: number } {
  console.log(arg, num)
  return { arg: { arg: "888" }, num: 668 };
}
console.log(identity({ arg: "222" }, 8989))
//为了达到一样的效果
function identity1<T>(arg: T): T {
  let res = { ar: "1", nu: 2 }
  return arg;
}
console.log(identity1({ arg: "222" }))




//枚举类型,会按规定顺序，否则是0123
enum Color { Red = 1, Green, Blue }
let c = Color[1];
console.log(c) //打印出Red
//元组声明
const strs: [string, string, [string]] = ['a', 'b', ['string']]
//任意声明，但是最好不要使用
let list: any[] = [1, true, "free"];

//ReadonlyArray适用于不可修改的数组，比如entity里面有个源数组，最好就使用这样的声明，表示它不可以被修改
const strs1: ReadonlyArray<string | Array<string>> = ['a', 'b', ['string']]
const strs2: ReadonlyArray<string | [string]> = ['a', 'b', ['string']]
// const strs2: String[] | Array[] = ['a', 'b', ['string']]


//但你比ts更熟悉某种类型的时候，可以使用类型断言
// 有两种，一种是as
let someValue: any = "this is a string";
let strLength: number = (someValue as string).length;
//另一种是尖括号 但是在tsx里面，只会被允许使用as，所以尖括号不必了解



class Student {
  fullName: string;
  // 在构造函数的参数上使用public等同于创建了同名的成员变量。如果不进行声明，它会自动进行推断
  constructor(public firstName: string, public middleInitial: string, public lastName: string) {
    this.fullName = firstName + " " + middleInitial + " " + lastName;
  }
}
let user = new Student("Jane", "M.", "User");
//类就是继承，然后继承函数声明的参数，相当于在同一个类里面声明，最后被打印出来为
console.log(user)
//Student {firstName: 'Jane', middleInitial: 'M.', lastName: 'User', fullName: 'Jane M. User'}



const greeter = (person: Person): string => {
  return "Hello, " + person.firstName;
}


function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        {greeter({ firstName: "firstName", })}

        <p>{calculator(199, 2, "add")}  </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <div>
          <ZJ
            counter={"111"}
          >
            {/* 这个就是props 的children */}
            1111
          </ZJ>


        </div>
      </header>
    </div>
  );
}

export default App;




//上面是一个简单的函数组件，在react里面，类组件会被用的广泛一些，但都有优势，下面是类组件的写法
//主要是对项目里面常用到的一些类型进行说明

type Operation = 'multiply' | 'add' | 'divide';

const calculator = (a: number, b: number, op: Operation): number | string | undefined => {
  if (op === 'multiply') {
    return a * b;
  } else if (op === 'add') {
    return a + b;
  } else if (op === 'divide') {
    if (b === 0) return 'can\'t divide by 0!';
    return a / b;
  } else {
    throw new Error('Operation is not multiply, add or divide!');
  }
}


try {
  //  console.log(calculator(1, 0 , '2'))  //这种情况下就会被catch到
  console.log(calculator(1, 0, 'divide'))
  //4.4版本的发布，默认类型变成了 unknown 。
} catch (error: unknown) {
  let errorMessage = 'Something went wrong.'
  if (error instanceof Error) {
    errorMessage += ' Error: ' + error.message;
  }
  console.log(errorMessage);
}



//类组件当中，我们可能犯如下错
// 试图传递一个额外的/不需要的props给一个组件
// 忘记把必需的props递给组件
// 将错误类型的属性传递给组件