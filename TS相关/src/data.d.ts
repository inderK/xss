//data.d.ts是一个只包含类型信息的类型声明文件



//接口类型
interface Person {
    firstName: string;
    lastName?: string;
    //如果有任意其他属性，就可以这么去声明
    [propName: string]: any;
}


//这里这么写也是可以的
// type Operation = 'multiply' | 'add' | 'divide';

//这是一个函数接口
interface SearchFunc {
    (string1: string, string2: string): boolean;
  }