
import React from 'react';

//定义类型也可以
type P = {
    counter: any,
    children?: any
}

//定义接口也可以
interface S {
    counter: any
}

//仅仅只是声明类型
interface a {
    // A: string;
    checkBatteryStatus(): void;
}

interface b {
    switchRadio(trigger: boolean): void;       //注意写法，void表示函数没有返回值
}

interface ab extends a {
    switchRadio(): void;
}

//实际实现
class A implements a {
    A: string;
    constructor(r: string, l?: string) { this.A = r + l }
    checkBatteryStatus() { console.log(this.A) }
}

class B implements b {
    switchRadio() { }
}
//类定义的继承可以继承多个类
class AB implements a, b {
    checkBatteryStatus() { }
    switchRadio() { }
}
//类只能扩展一个类
class AB1 extends A {
    private ab1: string;
    //如果构造函数不去修改这个类里面的属性，那么就是无用的构造，继承的所有构造函数都会运行
    constructor(r: string) {
        super(r)
        this.ab1 = r
    }
    checkBatteryStatus() { console.log(this.A + 1) }
    switchRadio() { }
}





class Chosen extends React.Component<P, S> {

    constructor(props: any) {
        super(props);
        // 不要在这里调用 this.setState()
        this.state = { counter: 0 };
        // this.handleClick = this.handleClick.bind(this);
    }


    render() {
        let AB11 = new AB1("AB1")
        console.log(AB11)



        
        console.log(this.props.children)
        return (
            <div>
                <div className="Chosen-select"
                // ref={el => this.el = el}
                >
                    ??? {this.props.children}
                </div>
            </div>
        );
    }
}

export default Chosen