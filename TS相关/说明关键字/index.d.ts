declare type Asd = {
    name: string;
}


declare module '*.css';
declare module '*.less';
declare module '*.png';

// declare就是告诉TS编译器你担保这些变量和模块存在，并声明了相应类型，编译的时候不需要提示错误！


// 声明一个作用域
// 声明完之后在其他地方的ts就可以直接API.ResponseList引用到这个接口类型
declare namespace API {
    interface ResponseList {}
}

//在这个文件里面顶级声明不用export的话，declare和直接写type、interface效果是一样的，在其他地方都可以直接引用
declare type Ass = {
    a: string;
}
type Bss = {
    b: string;
};