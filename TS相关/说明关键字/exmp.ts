// 类型继承
interface Person {
    name: string;
    age: number;
}
interface Player extends Person {
    item: 'ball' | 'swing';
}

//这样就有三个属性了
// 如果 T 可以满足类型 Person 则返回 Person 类型，否则为 T 类型
type IsPerson<T> = T extends Person ? Person : T;


// 在 TS 中用于类型表达时，typeof 可以用于从一个变量上获取它的类型。
const data = {
    value: 123,
    text: 'text',
    subData: {
        value: false
    }
};
type Data = typeof data;


function foo<T>(arg: T): number;
function foo(arg: number) {
    return arg

}
interface Persons<T> {
    name: "string",
    age: "number",
    phoneNum: T
}
type PersonProperty = keyof Person;
// type PersonProperty = "name" | "age" | "phoneNum"


// https://juejin.cn/post/6844904131975446536
// https://www.typescriptlang.org/play
// https://www.typescriptlang.org/docs/handbook/2/basic-types.html

function getProperty<T, K extends keyof T>(obj: T, key: K): T[K] {
    return obj[key];
}

function getProperty1<T>(obj: T, key: keyof T): T[keyof T] {
    return obj[key];
}
console.log(getProperty({ "BBB": 0 }, "BBB"))


type Property = 'name' | 'age' | 'phoneNum';
type PropertyObject = {
    [key in Property]: string;
}
let b: PropertyObject = {
    name: "string",
    age: "string",
    phoneNum: "0"
}



const y: Array<string> = []