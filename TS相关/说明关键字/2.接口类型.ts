// 属性类接口：对对象属性的约束
interface FullName {
    firstName: string; // 注意，用分号结束
    secondName: string;
}

function printName(name: FullName): void {
    // 必须传入对象，且带有属性：firstName，secondName，且属性值都是字符串
    console.log(`${name.firstName} -- ${name.secondName}`);
}
printName({ firstName: "小明", secondName: "小花" })


// 函数类接口：对方法传入的参数以及返回值进行约束，批量约束。
interface Encrytp {
    (key: string, val: string): string;
}

let md5: Encrytp = function (key: string, val: string): string {
    // 具体加密省略
    return key + val;
}
console.log(md5('name', 'Jane')); // nameJane







// https://blog.csdn.net/halations/article/details/122432280