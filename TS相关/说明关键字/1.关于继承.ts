

// 关于继承

// type和interface的区别
interface Name {
    name: string;
}
type Name1 = {
    name1: string;
}
interface User extends Name, Name1 {
    age: number;
}
type User1 = Name & Name1 & { age: number };

let a: User = {
    age: 1,
    name: "",
    name1: ""
}
let b: User1 = {
    age: 1,
    name: "",
    name1: ""
}


// 类型别名可以用于其它类型 （联合类型、元组类型、基本类型（原始值）），interface不支持
type PartialPointX = { x: number };
type PartialPointY = { y: number };

// union(联合) 
type PartialPoint = PartialPointX | PartialPointY;
let c: PartialPoint = {
    y: 1,
    x: 2,
    // z: 3  //不能将类型“{ y: number; x: number; z: number; }”分配给类型“PartialPoint”。
    // 对象文字可以只指定已知属性，并且“z”不在类型“PartialPoint”中。
}

// tuple(元祖) 
type Data = [PartialPointX, { y: number }];

let d: Data = [
    { x: 1 },
    { y: 2 }
    //必须按顺序
]

//primitive(原始值) 
type Name2 = Number;

// typeof的返回值 
let div = document.createElement('div');
type B = typeof div;

let e: B = [
    // { x: 1 },
    // 类型“any[]”缺少类型“HTMLDivElement”的以下属性: align, addEventListener, removeEventListener, accessKey 及其他 278 项。
]


// interface 可以多次定义 并被视为合并所有声明成员 type 不支持
interface Point {
    x: number;
}
interface Point {
    y: number;
}

const point: Point = { x: 1, y: 2 };

interface User0 {
    name: string;
    age: number;
}

interface User0 {
    sex: string;
}
//User接口为： 
//   { 
//       name: string; 
//       age: number; 
//       sex: string; 
//   } 



// type 能使用 in 关键字生成映射类型，但 interface 不行
type Keys = 'firstname' | 'surname';

type DudeType = {
    [key in Keys]: string;
};

const test: DudeType = {
    firstname: 'Pawel',
    surname: 'Grzybek',
}; 
