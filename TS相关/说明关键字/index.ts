// ?. 链判断运算符
const orderId = response?.result?.data?.orderId || '';
// 上面代码使用了?.运算符，直接在链式调用的时候判断，左侧的对象是否为null或undefined。如果是的，就不再往下运算，而是返回undefined。


type UploadType = 'drag' | 'select';
type SetUser = (name: string, age: number) => void;


