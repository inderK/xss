// async function async1() {
//     console.log('async1 start');
//     let p = await async2();
//     console.log(p);
//     console.log('async1 end');
// }

// async function async2() {
//     console.log('async2');
//     return new Promise(((resolve, reject) => {
//         console.log("??")
//         resolve(new Promise(((resolve, reject) => {
//             console.log("???")
//             resolve(10);
//         })).then((res) => {
//             console.log(2)
//             return res
//         }));
//     })).then((res) => {
//         console.log(1)
//         return res

//     })
// }
// console.log('script start');
// setTimeout(function () {
//     console.log('setTimeout');
// }, 0)
// async1();
// new Promise(function (resolve) {
//     console.log('promise1');
//     resolve();
// }).then(function () {
//     console.log('promise2');
// });
// console.log('script end');


var a = 10
var obj = {
  a: 20,
  say: () => {
    console.log(this.a)
  }
}
obj.say() 

var anotherObj = { a: 30 } 
obj.say.apply(anotherObj) 