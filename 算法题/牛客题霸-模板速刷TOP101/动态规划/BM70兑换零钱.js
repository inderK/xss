// 描述 给定数组arr，arr中所有的值都为正整数且不重复。每个值代表一种面值的货币，每种面值的货币可以使用任意张，再给定一个aim，代表要找的钱数，求组成aim的最少货币数。 如果无解，请返回-1.

// 数据范围：数组大小满足 0≤n≤10000 ， 数组中每个数字都满足 0<val≤10000，0≤aim≤5000


// https://blog.csdn.net/m0_46403734/article/details/120802026


function minMoney(arr, aim) {
    // write code here
    let dp = Array(aim + 1).fill(Infinity);
    dp[0] = 0;
    for (let item of arr) {
        for (let i = item; i <= aim; i++) {
            dp[i] = Math.min(dp[i], dp[i - item] + 1);
            console.log(dp)
        }
    }
    return dp[aim] === Infinity ? -1 : dp[aim];
}

console.log(minMoney([5, 2, 3], 20))