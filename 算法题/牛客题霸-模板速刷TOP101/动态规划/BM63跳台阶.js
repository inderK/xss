
// 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法（先后次序不同算不同的结果）。

// 数据范围：1 \leq n \leq 401≤n≤40
// 要求：时间复杂度：O(n)O(n) ，空间复杂度： O(1)O(1)

function jumpFloor(number) {
    // write code here
  
    if (number == 0) return 0;
    if (number == 1) return 1;
    if (number == 2) return 2;
  
    let list = [];
    list[0] = 0;
    list[1] = 1;
    list[2] = 2;
  
    for (let i = 3; i <= number; i++) {
      list[i] = list[i - 1] + list[i - 2];
    }
    return list[number];
  }
  module.exports = {
    jumpFloor: jumpFloor,
  };
  