// 一个机器人在m×n大小的地图的左上角（起点）。
// 机器人每次可以向下或向右移动。机器人要到达地图的右下角（终点）。
// 可以有多少种不同的路径从起点走到终点？
//m是有几行，n是有几列

// 11 12 13 . . . 1n
// 21 22 23 . . . 2n
// 31 32 33 . . . 3n
// . .
// .
// .
// 1m 2m          mn



// 要先定义一个二维数组，注意这里的mn是行数列数，一般定义二维数组的写法如下 
let list=new Array(m).fill(new Array(n).fill(0))
function uniquePaths(m, n) {
    // write code here
    let list = new Array(m).fill(new Array(n).fill(1));
    for (let i = 0; i <= m - 1; i++) {
      for (let j = 0; j <= n - 1; j++) {
        if (i > 0 && j > 0) {
          list[i][j] = list[i - 1][j] + list[i][j - 1];
        }
      }
    }
    return list[m - 1][n - 1];
  }
  module.exports = {
    uniquePaths: uniquePaths,
  };
  

































