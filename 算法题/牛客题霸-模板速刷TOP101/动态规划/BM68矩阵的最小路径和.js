// 给定一个 n * m 的矩阵 a，从左上角开始每次只能向右或者向下走，最后到达右下角的位置，路径上所有的数字累加起来就是路径和，输出所有的路径中最小的路径和。

// 数据范围: 1 \le n,m\le 5001≤n,m≤500，矩阵中任意值都满足 0 \le a_{i,j} \le 1000≤a 
// i,j
// ​
//  ≤100
// 要求：时间复杂度 O(nm)O(nm)

// 例如：当输入[[1,3,5,9],[8,1,3,4],[5,0,6,1],[8,8,4,0]]时，对应的返回值为12，



function minPathSum(matrix) {
    if (matrix.length == 0) return 0;
    let hang = matrix.length;
    let lie = matrix[0].length;
    let list = new Array(hang).fill(new Array(lie).fill(0));
    list[0][0] = matrix[0][0];
    //这里要注意边界条件，边界条件在于第一行和第一列是直接计算的，分情况if就行，不需要再构造多余的数组空间
    for (let i = 0; i < hang; i++) {
        for (let j = 0; j < lie; j++) {
            if (i == 0 && j == 0) continue;
            if (i == 0) {
                list[i][j] = list[i][j - 1] + matrix[i][j];
            } else if (j == 0) {
                list[i][j] = list[i - 1][j] + matrix[i][j];
            } else {
                list[i][j] = Math.min(
                    list[i - 1][j] + matrix[i][j],
                    list[i][j - 1] + matrix[i][j]
                );
            }
        }
    }
    return list[hang - 1][lie - 1];
}
module.exports = {
    minPathSum: minPathSum,
};
minPathSum([[1, 3, 5, 9], [8, 1, 3, 4], [5, 0, 6, 1], [8, 8, 4, 0]])