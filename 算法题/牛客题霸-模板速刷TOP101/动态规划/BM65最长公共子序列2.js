// 给定两个字符串str1和str2，输出两个字符串的最长公共子序列。如果最长公共子序列为空，则返回"-1"。目前给出的数据，仅仅会存在一个最长的公共子序列

// 输入："1A2C3D4B56","B1D23A456A"

// 返回值："123456"



/**
 * longest common subsequence
 * @param s1 string字符串 the string
 * @param s2 string字符串 the string
 * @return string字符串
 */
function LCS(s1, s2) {
    if (s1.length > s2.length) [s1, s2] = [s2, s1];
    const dp = new Array(s2.length + 1).fill("");
    //选取最长的字符串长度为数组长度 s1短，s2长，
    for (let i = 1; i <= s1.length; i++) {
        let pre = "";
        console.log(dp)
        for (let j = 1; j <= s2.length; j++) {
            //记下当前位置的备忘录字符串
            const tmp = dp[j];
            if (s1[i - 1] === s2[j - 1]) {
                // 如果s1的这个循环内，当前item和这s2的往后任意一个能相等，就把它和上一层循环内相同位置的字符串相互拼接，赋值到这个dp[i]上
                dp[j] = pre + s2[j - 1];
            } else {
                //如果不相等，就判断当前dp[i]的长度和上一个的长度，哪个长，就留下哪一个，因为上一个已经经过了循环，不可能再拼接其他字符了。
                dp[j] = dp[j].length > dp[j - 1].length ? dp[j] : dp[j - 1];
            }
            //把当前位置的备忘录字符串留存，下次循环的时候用来拼接相等的字符串，
            pre = tmp;
        }

    }
    const res = dp[s2.length];
    return res === "" ? "-1" : res;
}
module.exports = {
    LCS: LCS,
};
LCS("1A2C3D4B56", "B1123A456A")







