

// 给定两个字符串str1和str2,输出两个字符串的最长公共子串
// 题目保证str1和str2的最长公共子串存在且唯一。 

// 输入："1AB2345CD","12345EF"
// 返回值："2345"



function LCS(str1, str2) {
    // write code here
    if (str1.length === 0 || str2.length === 0) {
        return ''
    }
    if (str1.length > str2.length) { // 保证str1是最短的
        [str1, str2] = [str2, str1]
    }
    // 设置max为匹配的位数数=0，res=''为匹配的结果
    let max = 0, res = ''
    //从str1短的开始，第一位开始一个个匹配
    for (let i = 0; i < str1.length; i++) {
        //获取到的tmp为str1的一个初始区间，这个区间如果能够在str2匹配到，就会让右指针往后移动，如果匹配不到，就会让左指针往后移动，
        // 但考虑，如果匹配不到，那么只有一个指针移动，那么这个区间会越来越小
        //所以需要右指针无论如何都往右移动，如果一直匹配左指针可以停滞，如果有一项匹配不到了，左指针得向右移动，保持匹配数位不变的情况下，能够继续贪婪匹配
        const tmp = str1.slice(i - max, i + 1)
        if (str2.indexOf(tmp) >= 0) {
            res = tmp
            max++
        }
    }
    return res
}
module.exports = {
    LCS: LCS
};

LCS("1A2C3D4B56", "B1D23A456A")