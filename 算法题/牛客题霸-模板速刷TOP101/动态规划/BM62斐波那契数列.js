// 输入描述：
// 一个正整数n
// 返回值描述：
// 输出一个正整数。

function Fibonacci(n) {
    // write code here
  
    if (n == 1) {
      return 1;
    }
    if (n == 2) {
      return 1;
    }
    let list = [];
    list[0] = 0;
    list[1] = 1;
    list[2] = 1;
  
    for (let i = 3; i <= n; i++) {
      list[i] = list[i - 1] + list[i - 2];
    }
    return list[n];
  }
  module.exports = {
    Fibonacci: Fibonacci,
  };
  