// 给定一个整数数组 cost \cost  ，其中 cost[i]\cost[i]  是从楼梯第i \i 个台阶向上爬需要支付的费用，下标从0开始。一旦你支付此费用，即可选择向上爬一个或者两个台阶。

// 你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。

// 请你计算并返回达到楼梯顶部的最低花费。


function minCostClimbingStairs(cost) {
    /*
  *  dp数组记录每次爬到第i阶楼梯的最小花费
  *  dp[i] = min(dp[i-1]+cost[i-1], dp[i-2]+cost[i-2])
  *  dp[0] = 0  dp[1] = 0;
  *
  */
    if (cost.length < 2) return 0;
    let list = [];
    list[0] = 0;
    list[1] = 0;
    for (let i = 2; i <= cost.length; i++) {
        list[i] = Math.min(list[i - 1] + cost[i - 1], list[i - 2] + cost[i - 2]);
    }
    return list[cost.length];
}
module.exports = {
    minCostClimbingStairs: minCostClimbingStairs,
};




//递归解法；
// 这个不好
function minCostClimbingStairs(cost) {
    // write code here
  
    if (cost.length == 0) return 0;
    if (cost.length == 1) return 0;
  
    return Math.min(
      cost[cost.length - 1] + minCostClimbingStairs(cost.slice(0, cost.length - 1)),
      cost[cost.length - 2] + minCostClimbingStairs(cost.slice(0, cost.length - 2))
    );
  }
  module.exports = {
    minCostClimbingStairs: minCostClimbingStairs,
  };