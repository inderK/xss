// function res(x) {
//     // 负数的第一位带有负号'-'，所以一定不是回文数
//     // 长度超过一位的数字，第一位肯定不是 0，因此末尾是 0，则一定不是回文数
//     // if (num < 0 || (num % 10 == 0 && num != 0))
//     // return false;
//     // 余下的代码，如果看不懂原理，就带入几个具体的数字，手工模拟运行过程，就很容易明白了
//     if (x < 0 || x != 0 && x % 10 == 0) {
//         return false;
//     }
//     let reverse = 0;
//     while (x > reverse) {
//         reverse = reverse * 10 + x % 10;
//         x = x / 10;
//     }
//     return ((x == reverse) || (reverse / 10) == x);
// }


// let num = 121

// console.log(res(num))



// 证明回文数
function pp(str) {
    return str === str.split("").reverse().join("")
}
// console.log(pp("112211"))

//最后回文数
function ppmax(str) {
    // return str.slice(3)
    if (str === "" || str.length == 1) { return true }
    for (let i = str.length + 1; i > 0; i--) {
        if (pp(str.slice(0, i))) { return str.slice(0, i) }
    }0
    return false
}

console.log(ppmax("9848646146411"))