
let a = [5, 2, 8]
let b = [1, 2, 3];
function TreeNode(left, right, index, level) {
    this.left = left;
    this.right = right;
    this.total = index == -1 ? 0 : a[index] * b[level];
    console.log(this.total, index, level)
}
const treeMap = (leftIndex, rightIndex, fatherNode, index, level = -1) => {
    let left = null;
    let right = null;
    if (rightIndex - leftIndex >= 0) {
        left = treeMap(leftIndex + 1, rightIndex, fatherNode, leftIndex, level + 1);
        right = treeMap(leftIndex, rightIndex - 1, fatherNode, rightIndex, level + 1);
    }
    // 生成父节点
    let node = new TreeNode(left, right, index, level);
    return node;
}


let fatherNode = new TreeNode(null, null, -1);
let listLen = a.length - 1;

// 取第一个述作为主节点
let tree = treeMap(0, listLen, fatherNode, -1);

let valueResult = []
const deepLoop = (node, father = 0) => {
    if (!node) {
        valueResult.push(father);
        return;
    }
    console.log(node.total)
    const value = node.total ? node.total + father : father
    deepLoop(node.left, value);
    deepLoop(node.right, value);

}
deepLoop(tree);
let max = 0;
valueResult.map(item => {
    if (max < item) {
        max = item;
    }
})

console.log(max, valueResult, JSON.stringify(tree));