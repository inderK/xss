
function useArguments() {
    // [Arguments] { '0': 123, '1': 56, '2': 545 }
    // 特殊数组，可以使用数组的基本属性，但是没有函数调用
    console.log(arguments)
    let sum = 0;
    for(let i = 0; i < arguments.length; i++){
        sum += arguments[i];
    }
    return sum;
}
console.log(    useArguments(123,56,545)   )


// 获取数字 num 二进制形式第 bit 位的值
function valueAtBit(num, bit) {
    let Num2Str = num.toString(2)
    return Num2Str[Num2Str.length - bit]
}

