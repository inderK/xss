// 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法（先后次序不同算不同的结果）。

// 数据范围：1 \leq n \leq 401≤n≤40
// 要求：时间复杂度：O(n)O(n) ，空间复杂度： O(1)O(1)

// 问题描述：青蛙可跳1-2级台阶，现在需要跳n个台阶，共有多少中跳法

// 问题解析：设共跳x个1级台阶，y个2级台阶，可推出x+2y=n =>x=n-2y,
// 最终问题为对n-2y个一级台阶与y个2级台阶排列组合，
// 即C(n-y,y)。y的范围：y>=0&&y<=(n/2)  x的范围：x>=0&&x<=0。


function jumpFloor(number) {
    // write code here
    // let two; /*跳两级台阶的次数*/
    let count = 0;
    for (let two = 0; two <= number / 2; two++) {
        count += com(number - two, two);
    }
    return count;
}
//从m中取n个进行排列组合 
const com = (m, n) => {
    let i = m;
    let sum = 1;
    for (let j = 0; j < n; j++, i--) {
        sum = (sum * i) / (j + 1);
    }
    return sum;
};
module.exports = {
    jumpFloor: jumpFloor,
};

