
// 在一个二维数组array中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
// [
// [1,2,8,9],
// [2,4,9,12],
// [4,7,10,13],
// [6,8,11,15]
// ]
// 给定 target = 7，返回 true。

// 给定 target = 3，返回 false。

// 数据范围：矩阵的长宽满足 0 \le n,m \le 5000≤n,m≤500 ， 矩阵中的值满足 0 \le val \le 10^90≤val≤10 
// 9

// 进阶：空间复杂度 O(1)O(1) ，时间复杂度 O(n+m)O(n+m)


function Find(target, array) {
    // write code here
    let res = false
    array.map((item) => {
        item.map((i) => {
            if (i == target) {
                res = true
            }
        })
    })
    return res
}
module.exports = {
    Find: Find
};

// 进阶          按照右上角进行
function Find(target, array) {
    /*判断数组是否为空*/
    let hangshu = array.length;
    if (!hangshu) { return false }
    let lieshu = array[0].length;
    if (!lieshu) { return false }


    let x = 0
    let y = array[0].length


    while ((x <= hangshu) && (y >= 0)) {
        if (array[x][y] === target) {
            return true
        } else if (array[x][y] > target) {
            y = y - 1
        } else {
            x = x + 1
        }
    }
    return false
}

module.exports = {
    Find: Find
}

//进阶   按照左下角开始
function Find(target, array) {
    /*判断数组是否为空*/
    let hangshu = array.length;
    if (!hangshu) {
        return false;
    }
    let lieshu = array[0].length;
    if (!lieshu) {
        return false;
    }

    let x = array.length - 1;
    let y = 0;

    while (x >= 0 && y <= lieshu - 1) {
        if (array[x][y] === target) {
            return true;
        } else if (array[x][y] > target) {
            x = x - 1;
        } else {
            y = y + 1;
        }
    }

    return false;
}
module.exports = {
    Find: Find,
};














