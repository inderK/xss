// 请实现无重复数字的升序数组的二分查找
// 给定一个 元素升序的、无重复数字的整型数组 nums 和一个目标值 target ，写一个函数搜索 nums 中的 target，如果目标值存在返回下标（下标从 0 开始），否则返回 -1
// 数据范围：0 \le len(nums) \le 2\times10^50≤len(nums)≤2×10 
// 5
//   ， 数组中任意值满足 |val| \le 10^9∣val∣≤10 
// 9

// 进阶：时间复杂度 O(\log n)O(logn) ，空间复杂度 O(1)O(1)


// nums = []
// nums1 = [-1, 0, 3, 4, 6, 10, 13, 14]
// target = 13
// function search(nums, target) {
//     let res = -1
//     nums.map((item, index) => {
//         if (item === target) { res = index }
//     })
//     return res
// }

// module.exports = {
//     search: search
// }
// console.log(search(nums1, target))


// 进阶
nums = []
nums1 = [-1, 0, 3, 4, 6, 10, 13, 14]
target = 13
function search(nums, target) {
    let left = 0;
    //尽量让它指向索引
    let right = nums.length - 1;
    //用的是向下取整，所以最多就是到达right，所以不会超出去取到数组外面
    //考虑边界条件，处于数组最右边，左指针大于右指针之前，左指针等于右指针，还没搜索到，就可以判定数组里面没有target，可以跳出去
    //如果到边界了还没有搜索到，判断大小后就进不去循环了，满足边界条件
    while (left <= right) {

        let mid = (left + right) >> 1
        if (nums[mid] === target) { return mid }
        if (nums[mid] > target) { right = mid - 1 }
        if (nums[mid] < target) { left = mid + 1 }
    }
    return -1
}

module.exports = {
    search: search
}


console.log(search(nums1, target))






