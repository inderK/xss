// 给定一个长度为n的数组nums，请你找到峰值并返回其索引。数组可能包含多个峰值，在这种情况下，返回任何一个所在位置即可。
// 1.峰值元素是指其值严格大于左右相邻值的元素。严格大于即不能有等于
// 2.假设 nums[-1] = nums[n] = -\infty−∞
// 3.对于所有有效的 i 都有 nums[i] != nums[i + 1]
// 4.你可以使用O(logN)的时间复杂度实现此问题吗？

// 数据范围：
// 1 \le nums.length \le 2\times 10^5 \1≤nums.length≤2×10 
// 5

// -2^{31}<= nums[i] <= 2^{31} - 1−2 
// 31
//  <=nums[i]<=2 
// 31
//  −1

// 如输入[2,4,1,2,7,8,4]时，会形成两个山峰，一个是索引为1，峰值为4的山峰，另一个是索引为5，峰值为8的山峰




//  //完全遍历
// function findPeakElement(nums) {
//     // write code here
//     if (nums.length <= 1) return 0
//     if (nums[0] > nums[1]) return 0
//     if (nums[nums.length - 1] > nums[nums.length - 2]) return nums.length - 1
//     for (let i = 1; i < nums.length; i++) {
//         if (nums[i] > nums[i + 1] && nums[i] > nums[i - 1])
//             return i
//     }
// }
// module.exports = {
//     findPeakElement: findPeakElement
// };

//进阶
function findPeakElement(nums) {
    // write code here
    let n = nums.length;
    let left = 0, right = n - 1;
    while (left < right) {
        let mid = left + ((right - left) >> 1);
        if (nums[mid] < nums[mid + 1]) left = mid + 1;
        else right = mid;
    }
    return left
}
module.exports = {
    findPeakElement: findPeakElement
};

findPeakElement([2, 4, 1, 2, 7, 8, 4])