let a = [5, 2, 8, 9, 8, 2, 3, 4, 5, 2, 8,]
let b = [1, 2, 3, 5, 6, 1, 5, 2, 1, 5, 1,];


// let a = [5, 2,8]
// let b = [1, 2,3];

//备忘录+递归
// function res(a = [], b = []) {
//     let list = []
//     if (a.length == 0) { return 0 }
//     if (a.length == 1) { return a[0] * b[0] }
//     list[0] = 0
//     list[1] = a[0] * b[0]
//     list[2] = Math.max(a[0] * b[0] + a[1] * b[1], a[1] * b[0] + a[0] * b[1])
//     list[a.length] = Math.max(
//         a[0] * b[0] + res(a.slice(1), b.slice(1)),
//         a[a.length - 1] * b[0] + res(a.slice(0, a.length - 1), b.slice(1))
//     )
//     return list[a.length]
// }
// console.log(res(a, b))


//备忘录+非递归
function res1(a = [], b = []) {
    let list = []
    if (a.length == 0) { return 0 }
    if (a.length == 1) { return a[0] * b[0] }
    list[0] = 0
    list[1] = a[0] * b[0]
    list[2] = Math.max(a[0] * b[0] + a[1] * b[1], a[1] * b[0] + a[0] * b[1])


    for (let i = 3; i <= a.length; i++) {
        list[i] = Math.max(
            list[i - 1] + a[a.length - 1] * b[b.length - 1],    //这么写已经是违反了题目规则
            a[a.length - 1] * b[0] + res1(a.slice(0, a.length - 1), b.slice(1))
        )

    }
    //所以这样思考的话，这个问题应该是分解不出其他子问题了，也没有重复计算的地方，必须深度遍历
    return list[a.length]
}
console.log(res1(a, b))
//331  是错的，比较高是正常的