// 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法（先后次序不同算不同的结果）。

// 数据范围：1 \leq n \leq 401≤n≤40
// 要求：时间复杂度：O(n)O(n) ，空间复杂度： O(1)O(1)

// 比较倾向于找规律的解法，f(1) = 1, f(2) = 2, f(3) = 3, f(4) = 5，  可以总结出f(n) =f(n-1) +f(n-2)的规律，
// 假设现在6个台阶，
// 我们可以从第5跳一步到6，这样的话有多少种方案跳到5就有多少种方案跳到6，另外我们也可以从4跳两步跳到6，跳到4有多少种方案的话，就有多少种方案跳到6，
// 其他的不能从3跳到6什么的啦，所以最后就是f(6)= f(5) + f(4)；

//递归
function jumpFloor(number) {
    if (number < 1) { return 0; }
    if (number == 1) { return 1; }
    if (number == 2) { return 2; }
    return jumpFloor(number - 1) + jumpFloor(number - 2);
}
console.log(jumpFloor(10))

//备忘录+递归
let list = []
function jumpFloor1(number) {
    if (number < 1) { list[0] = 0; return 0; }
    if (number == 1) { list[1] = 1; return 1; }
    if (number == 2) { list[2] = 2; return 2; }
    if (list[number]) { return list[number] }
    list[number] = jumpFloor1(number - 1) + jumpFloor1(number - 2)
    return list[number]
}
console.log(jumpFloor1(100))


//备忘录+非递归
function jumpFloor2(number) {
    if (number < 1) { list[0] = 0; return 0; }
    if (number == 1) { list[1] = 1; return 1; }
    if (number == 2) { list[2] = 2; return 2; }
    for (let i = 3; i <= number; i++) {
        list[i] = list[i - 1] + list[i - 2]
    }
    return list[number]
}
console.log(jumpFloor2(100))


//备忘录+双指针优化空间
function jumpFloor3(number) {
    if (number < 1) { list[0] = 0; return 0; }
    if (number == 1) { list[1] = 1; return 1; }
    if (number == 2) { list[2] = 2; return 2; }
    let pre = 1
    let mid = 2
    let next = 3
    for (let i = 3; i <= number; i++) {
        next = mid + pre
        pre =mid
        mid = next
    }
    return next
}
console.log(jumpFloor3(100))


module.exports = {
    jumpFloor: jumpFloor,
};
