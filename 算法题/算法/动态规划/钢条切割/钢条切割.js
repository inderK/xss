
let list = [1, 5, 8, 9, 10, 17, 17, 20, 24, 30]
// 这一题的局限在于，输入的钢条长度不能大于价格区间,有错有错，先不改了


//递归
function res(p, n) {
    if (n == 0) return 0;
    let q = 0;
    for (let i = 1; i <= n; i++) {
        q = Math.max(q, p[i - 1] + res(p, n - i));
    }
    // console.log(q)
    return q;
}
console.log(res(list, 10))


//备忘录
let r = new Array(100).fill(-1)
function cut(p, n) {
    let q = -1;
    if (r[n] >= 0)
        return r[n];
    if (n == 0)
        q = 0;
    else {
        for (let i = 1; i <= n; i++) {
            q = Math.max(q, cut(p, n - i) + p[i - 1]);
        }
    }
    r[n] = q;
    return q;
}
console.log(cut(list, 10))


//自底向上

function buttom_up_cut(p) {
    let r = new Array(100).fill(-1)
    for (let i = 1; i <= p.length; i++) {
        let q = -1;
        for (let j = 1; j <= i; j++)
            q = Math.max(q, p[j - 1] + r[i - j]);
        r[i] = q;
    }
    return r[p.length];
}
console.log(buttom_up_cut(list, 10))