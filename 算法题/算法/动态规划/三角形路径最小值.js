// 给定一个正三角形数组，自顶到底分别有 1，2，3，4，5...，n 个元素，找出自顶向下的最小路径和。
// 每一步只能移动到下一行的相邻节点上，相邻节点指下行种下标与之相同或下标加一的两个节点。
// 数据范围：三角形数组行数满足 1 \le n \le 200 \1≤n≤200  ，数组中的值都满足 |val| \le 10^4 \∣val∣≤10 
// 4

// 例如当输入[[2],[3,4],[6,5,7],[4,1,8,3]]时，对应的输出为11，



// 从下往上原地动态规划，将原始的triangle数组视为dp数组。
// dp[i][j]表示从最底层的三角形累加到triangle[i][j]时所得到的最小值，因此它的值应该为triangle[i][j]加上它正下方和右下方中小的那个，
// 状态转移方程为：dp(i,j) =triangle(i,j)+min(dp(i+1),j,dp(i+1,j+1))



function minTrace(triangle) {
    let n = triangle.length;
    for (let i = n - 2; i >= 0; i--) {
        let m = triangle[i].length;
        for (let j = 0; j < m; j++) {
            triangle[i][j] += Math.min(triangle[i + 1][j], triangle[i + 1][j + 1]);
        }
    }
    return triangle[0][0];
}
module.exports = {
    minTrace: minTrace
};
console.log(
    minTrace([[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]])
)


/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 *
 * @param triangle let整型二维数组
 * @return let整型
 */
// function mletrace(triangle) {
//     // write code here
//     const n = triangle.length;
//     const dp = new Array(n).fill(null).map(() => new Array(n).fill(Infinity));
//     console.log(dp)
//     dp[0][0] = triangle[0][0];
//     for (let i = 1; i < n; i++) {
//         for (let j = 0; j <= i; j++) {
//             if (j - 1 >= 0) dp[i][j] = Math.min(dp[i][j], dp[i - 1][j - 1] + triangle[i][j]);
//             if (j <= i - 1) dp[i][j] = Math.min(dp[i][j], dp[i - 1][j] + triangle[i][j]);
//         }
//     }
//     return Math.min(...dp[n - 1]);
// }
// module.exports = {
//     mletrace: mletrace
// };


