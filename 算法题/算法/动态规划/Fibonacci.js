let number = 8

// 最简单的递归,一旦number==100，那么这个算法会卡死,
function res1(num) {
    if (num == 0) { return 0 }
    if (num == 1) { return 1 }
    return res1(num - 1) + res1(num - 2)
}
console.log(res1(number))


// ①自顶向下的备忘录法
// 这个是从上到下，先算出n的值，然后向下继续算n-1和n-2，这样最后算出来最底下的值，再递归返回赋值到数组里面，这样的话产生了额外的开销
//let list = Array(number + 1).fill(null)
let list = []
function res2(num) {
    if (list[num] != null) { return list[num] }
    if (num <= 2) {
        return 1
    } else {
        list[num] = res2(num - 1) + res2(num - 2)
    }
    // console.log(list)
    return list[num]
}
// console.log(res2(number))


// ②自底向上的动态规划
// 上面那个方法也是属于算一个就要算最下面的那个，虽然有直接保存不用重复算，但不如直接就从最小的算起来，算到目标位置就停下来，关键就是不使用递归
// 也就是动态规划的核心，先计算子问题，再由子问题计算父问题。
function res3(num) {
    if (num <= 2) { return 1 }
    let list = []
    list[0] = 0
    list[1] = 1
    for (let i = 2; i <= num; i++) {
        list[i] = list[i - 1] + list[i - 2]
    }
    return list[num]
}
// console.log(res3(number))


//可以利用移动指针进行空间上的优化
function res4(num) {
    if (num <= 2) { return 1 }
    let pre = 1
    let mid = 1
    let next = 2
    for (let i = 3; i <= num; i++) {
        next = pre + mid
        pre = mid
        mid = next
    }
    return next
}
// console.log(res4(number))


