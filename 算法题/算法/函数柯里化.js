// 返回值为一个函数 f,返回值为按照调用顺序的参数拼接，拼接字符为“,”
// 一个闭包完美解决
function functionFunction(str) {
    return f = function (arr) {
        return str + ", " + arr;
    };

}

// console.log(functionFunction('Hello')('world'))


function curryIt(fn) {
    let args = []  
 
    return function curried(arg) {
        args.push(arg)      
        if (args.length >= fn.length) {
            return fn.apply(this, args)
        } else {
            return function(arg2) {  
                return curried.call(this, arg2)
            }
        }
    }
}


var fn = function (a, b, c) {return a + b + c}; 
console.log(
    curryIt(fn)(1)(2)(3)
)

// 6