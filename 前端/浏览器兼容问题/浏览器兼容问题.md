不同浏览器的标签默认的外补丁和内补丁不同
问题表现：随便写几个标签，不加样式控制的情况下，各自的margin 和padding差异较大。
解决方案：CSS里    *{margin:0;padding:0;}
备注：这个是最常见的也是最易解决的一个浏览器兼容性问题，几乎所有的CSS文件开头都会用通配符*来设置各个标签的内外补丁是0。


1、标准的事件绑定方法函数为addEventListener，但IE下是attachEvent；
2、事件的捕获方式不一致，标准浏览器是由外至内，而IE是由内到外，所以一般使用冒泡阶段的事件
3、placeholder在IE9及以下是不兼容的，有些属性在IE低版本是
4、属性级Hack属性值，可以设置适用于IE或者其他浏览器的属性值，但是Hack有风险，使用需谨慎 英[hæk]
5、HTML内可以使用条件判断，用于分辨不同的浏览器
6、window.event获取的。并且获取目标元素的方法也不同，标准浏览器是event.target，而IE下是event.srcElement
7、ajax的实现方式不同，这个我所理解的是获取XMLHttpRequest的不同，IE下是activeXObject
8、DOM节点的父节点、子节点的方式在IE浏览器和谷歌浏览器中也有部分不同