
var timer
var nowtimer
var time
var myBarwidth
var kaigaun = true
var width = 0


// 这个设置了就会运行，不能用函数进行代替，代替了还是会运行的
// 设置并且运行运行一次的计时器
function onclickstart() {
    timer = setTimeout(function () { alert("在一秒内按下停止异步的按钮就可以阻止弹窗"); }, 1000);
}
//点击卸载一次
function onclickstop() { clearTimeout(timer); }


// 设置运行无数次的计时器,获取当前时间,并且放进span里面
function showtime() {
    time = new Date()
    time = time.toLocaleTimeString()
    document.getElementById("time").innerText = time
    document.body.style.backgroundColor = document.body.style.backgroundColor == "red" ? "yellow" : "red"
}

// 点击显示当前时间，不停地显示
function runtimers() {
    nowtimer = setInterval(function () { showtime() }, 1000);
}

// 点击取消显示时间，会停止这个时间
function stopruntimers() {
    clearInterval(nowtimer);
}


//设置动态进度条
function showmyBar() {
    let myBar = document.getElementById("myBar")
    if (kaigaun) {
        myBarwidth = setInterval(() => {
            if (width == 100) {
                width = 0
            } else {
                width++;
            }
            myBar.style.width = width + "%"

        }, 10)

        kaigaun = false
    } else {
        clearInterval(myBarwidth)
        kaigaun = true
    }
}





//缓存举例，但是暂时不会知道为何不生效
function readcache() {

    // window.sessionStorage.setItem("token", tex)
    // window.sessionStorage.removeItem("token")

    // 好像如果没有放到服务器上就不能运行存储
    var user = getCookie("username");
    if (user != "") {
        alert("欢迎 " + user + " 再次访问");
    }
    else {
        user = prompt("请输入你的名字:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 30);
        }
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    console.log(cname + "=" + cvalue + "; " + expires)
    document.cookie = cname + "=" + cvalue + "; " + expires + "path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) { return c.substring(name.length, c.length); }
    }
    return "";
}
