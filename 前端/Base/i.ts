// 获取数组里面元素的类型
type getItemType<T> = T extends Array<infer E> ? E : T

type ItemType = Array<{ a: number }>

type target = getItemType<ItemType>


type getItemType1<T> = T extends { a: infer U, b: infer U } ? U : never

type ItemType1 = { a: number, b: string }

type target1 = getItemType1<ItemType1>






// keyof
type Person = {
    id: number;
    name: string;
    age: number;
};

type P1 = keyof Person; //'id' | 'name' | 'age'
type P2 = Person[keyof Person]


type MyPick<T, K extends keyof T> = { [P in K]: T[P] };
type P3 = MyPick<Person, 'id' | 'age'>
type MyPick1<T, K extends keyof T> = T[K] 
type P31 = MyPick1<Person, 'name'>