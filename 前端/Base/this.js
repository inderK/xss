
将函数 fn 的执行上下文改为 obj 对象

方法一:直接将fn挂载到obj对象上
function speak(fn, obj) {
    obj.fn = fn
    return obj.fn()
}
方法二:使用apply
function speak(fn, obj) {
    return fn.apply(obj)
}

方法三:使用call
function speak(fn, obj) {
    return fn.call(obj)
}
方法四:使用bind
function  speak(fn, obj) {
    return fn.bind(obj)()
}