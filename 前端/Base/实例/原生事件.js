window.onload = () => {
  let findField = document.getElementById("a");
  // console.log(findField);
  let div = document.getElementById("div");
  let div11 = document.getElementById("div11");
  let myList = document.getElementById("myList");
  let div22 = document.createTextNode('div22');

  // document.body.addEventListener("click", () => { console.log("body冒泡的发生") }, false)

  //这种采用的是事件冒泡方式，安装监听器会先捕获再冒泡，可选在哪个过程中触发   attachEvent是在IE中注册事件的方法，IE只支持事件冒泡
  div11.onclick = (e) => {

    // let div0 = document.createTextNode('div');
    // div0.id = "div1"
    // div0.style.backgroundColor = 'pink';
    // div0.innerHTML = '这是新创建的div0';
    // document.getElementsByTagName("")    //通过标签名称
    // document.getElementsByName("")    //通过元素的Name属性的值
    // document.getElementById("")    //通过元素Id，唯一性
    // document.getElementsByClassName("");  //通过类查找
    // document.querySelector("#div11")    //css的id查找

    // - createDocumentFragment() //创建一个 DOM 片段
    // - createElement() //创建一个具体的元素
    // - createTextNode() //创建一个文本节点


    // let node = div.cloneNode(true)      //true会复制所有子节点，false只会复制当前结点
    // document.body.appendChild(node)                //插入一个元素
    // myList.insertBefore(div22, myList.childNodes[0])  //只能插入到子元素里面，insertAfter//这个方法在原生里面没有
    // div1.removeChild(div1.childNodes[0])          //只能移除本身的子元素
    // div1.replaceChild(div22, div1.childNodes[0])           //old_child.replaceChild(new_child,old_child) 只能替换本身下面的子元素


    // e.preventDefault();
    // e.stopPropagation(); 

    return false   //事件里面可以阻止默认行为，但冒泡不会阻止，jQuery里面既阻止默认行为又停止冒泡

  }

  //这两者监听器是一样的，谁在前面，就会先运行谁,onClick的运行是在冒泡阶段，相当于false，这种方法没办法卸载
  findField.onclick = (e) => {
    // e.preventDefault();

    // console.log(document.activeElement)//返回当前文档中被击活的标签节点(ie) 比如点击了连接就返回连接

    // console.log(div.parentNode)//返回父标签，一样的，推荐，符合W3C
    // console.log(div.parentElement)//返回父标签，一样的

    // console.log(div.childNodes)//返回所有子节点，包括文本结点和子节点，推荐，符合W3C  childNodes[0]是Node对象
    // console.log(div.children)//返回所有的子对象，不包括文本结点的！！并列，[0]是Node对象

    // console.log(findField.firstChild)//返回第一个子节点，文本也算是节点,是Node对象
    // console.log(findField.lastChild)//返回最后一个子节点，是Node对象
    // console.log(div.nextSibling)//返回同级下一个子节点
    // console.log(div.previousSibling)//返回同级上一个节点，文本也算是节点

    // 下面是浏览器兼容-------------------------------------------------
    // 阻止浏览器的继续冒泡，但是当前的事件还是会运行的
    // if (e && e.stopPropagation)
    //   e.stopPropagation();  //因此它支持W3C的stopPropagation()方法 
    // else{
    //   window.event.cancelBubble = true;   //否则，我们需要使用IE的方式来取消事件冒泡 
    // }

    //阻止默认浏览器动作(W3C) 比如链接跳转，但冒泡不会
    // if (e && e.preventDefault)
    // e.preventDefault();
    // else {
    //   window.event.returnValue = false;    //IE中阻止函数器默认动作的方式 
    // }

  }
  // 捕获阶段、目标阶段和冒泡阶段，其中如果设置了两个监听事件，一个是冒泡阶段一个是捕获阶段，都在目标元素的话，会先执行捕获的，再执行冒泡的
  // 当然，如果在同一个阶段进行监听又卸载，是没有机会运行监听指定的函数的

  //在捕获阶段就会运行的监听函数，卸载还没来得及
  // findField.addEventListener("click", foo, true);//false表示监听一直存在，触发以后在冒泡阶段运行
  // findField.removeEventListener("click", foo, false);//true表示在捕获阶段就卸载这个监听函数，导致在冒泡阶段运行的监听函数还没启动就被卸载了


  //在捕获阶段就卸载了的监听函数，运行还没来得及
  // findField.addEventListener("click", foo, false);//false表示监听一直存在，触发以后在冒泡阶段运行
  // findField.removeEventListener("click", foo, true);//true表示在捕获阶段就卸载这个监听函数，导致在冒泡阶段运行的监听函数还没启动就被卸载了

  // W3C模型运行顺序是，先捕获，从document进去到触发的元素里，然后从元素再上升到document,如果要逐层监听，可以使用document.boby等元素来设置





};
function foo(e) {
  console.log(e)
  window.alert("666")
  // e.preventDefault();  //阻止默认行为，不能阻止冒泡
  // e.stopPropagation()  //这个可以阻止冒泡继续进行，无论是在捕获阶段还是冒泡阶段，都可以阻止进去冒泡或者继续冒泡
  // return false         //阻止默认行为，不能阻止冒泡
}









