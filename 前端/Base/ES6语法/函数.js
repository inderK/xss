传参
默认值
不定参数("a",...res)  //用来解构
// 箭头函数，直接指向当前this


注意事项
箭头函数 "=>" 是es6的写法
//es6写法
setTimeout(() => {
    console.log(" 3秒 超时的输出内容！");
}, 3000);

//常规写法
setTimeout(function () {
    console.log(" 3秒 超时的输出内容！");
}, 3000);


// arguments是传入的所有参数


function foo() {}
// 这是方法定义，会在运行之前就解析
var foo = function() {};
// 这是赋值语句，只会在运行的时候按循序运行

// 回调地狱
https://blog.csdn.net/qq_21602341/article/details/87820778
