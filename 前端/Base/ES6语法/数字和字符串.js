Math.pow(item,2)            //数学运算，2次方
// 字符串和数字相互转换
    let c=11
    c.toString()    //"11"
    c.toFixed(2)    //"11.00" 自动四舍五入
Number('-0')    //-0
String(55)  //'55'
parseInt("0111.11ssda1",10) //111 按照十进制进行解析  ,默认是10  
parseInt("0111.11ssda1") //111  只解析整数  会保留符号
parseFloat(".1aa")  //0.1  可以解析浮点数   会保留符号




// 另外0.1+0.2无法精确表达是因为二进制的原因，在这个网站中也说了。二进制只能精准表达2除尽的数字1/2, 1/4, 1/8，例如0.1(1/10)和0.2(1/5)，在二进制中都无法精准表示时，需要根据精度舍入。我们人类熟悉的十进制运算系统，可以精准表达2和5除尽的数字，例如1/2, 1/4, 1/5(0.2), 1/8, 1/10(0.1)。当然十进制也有无法除尽的地方，例如1/3, 1/7，也需要根据精度舍入。
0.1 + 0.2 === 0.3; // false
// 在误差范围内即视为相等
equal = (Math.abs(0.1 - 0.3 + 0.2) < Number.EPSILON); // true

// 安全整数
// 安全整数表示在 JavaScript 中能够精确表示的整数，安全整数的范围在 2 的 -53 次方到 2 的 53 次方之间（不包括两个端点），超过这个范围的整数无法精确表示。
// 最大安全整数
// 安全整数范围的上限，即 2 的 53 次方减 1 。
Number.MAX_SAFE_INTEGER + 1 === Number.MAX_SAFE_INTEGER + 2; // true
Number.MAX_SAFE_INTEGER === Number.MAX_SAFE_INTEGER + 1;     // false
Number.MAX_SAFE_INTEGER - 1 === Number.MAX_SAFE_INTEGER - 2; // false
// 最小安全整数
// 安全整数范围的下限，即 2 的 53 次方减 1 的负数。
Number.MIN_SAFE_INTEGER + 1 === Number.MIN_SAFE_INTEGER + 2; // false
Number.MIN_SAFE_INTEGER === Number.MIN_SAFE_INTEGER - 1;     // false
Number.MIN_SAFE_INTEGER - 1 === Number.MIN_SAFE_INTEGER - 2; // true
// 记住2的53次方两边是不包括的，在内都是暗转整数，可以被判断

// 简单运算：
判断整数
取整
Math.trunc(12.3); // 12
Math.trunc(12);   // 12
// 整数部分为 0 时也会判断符号
Math.trunc(-0.5); // -0
Math.trunc(0.5);  // 0
// Math.trunc 会将非数值转为数值再进行处理
Math.trunc("12.3"); // 12
// 空值或无法转化为数值时时返回 NaN
Math.trunc();           // NaN
Math.trunc(NaN);        // NaN
Math.trunc("hhh");      // NaN
Math.trunc("123.2hhh"); // NaN
1.丢弃小数部分, 保留整数部分
parseInt(5 / 2)   //2
parseInt(-1.9)  //-1
2.向上取整, 有小数就整数部分加1
Math.ceil(5 / 2)   //3
Math.ceil(-2.9)  //-2
3, 四舍五入.
    Math.round(2.5)    //3
Math.round(-2.6)   //-3
4, 向下取整
Math.floor(5 / 2)    //2
Math.floor(-2.1)   //-3

精确到几位
num.toFixed(2)  //精确到小数点后的两位，但这个返回的是字符串

判断在正负符号
Math.sign(1);  // 1
Math.sign(-1); // -1
// 参数为 0 时，不同符号的返回不同
Math.sign(0);  // 0
Math.sign(-0); // -0
// 判断前会对非数值进行转换
Math.sign('2');  // 1
Math.sign('-2'); // -1  
// 参数为非数值（无法转换为数值）时返回 NaN
Math.sign(NaN);   // NaN 
Math.sign('hhh'); // NaN

指数运算符
1 ** 2; // 1
// 右结合，从右至左计算
2 ** 2 ** 3; // 256
// **=
let exam = 2;
let res = exam ** = 2; // 4



// 二进制转换
// 将给定数字转换成二进制字符串。如果字符串长度不足 8 位，则在前面补 0 到满8位。
function convertToBinary(num) {
    let a = num.toString(2)
    let i = 8 - a.length
    let str0 = ''
    for (; i > 0; i--) {
        str0 += '0'
    }
    return str0 + a
}