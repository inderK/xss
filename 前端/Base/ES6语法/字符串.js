var string = "apple,banana,orange";
// 某字符在字符串里的位置
// 第一次出现
string.indexOf("p")         //1，不存在是-1
//最后一次出现
string.lastIndexOf("p")   //2，不存在是-1
// 插入变量
var string2 = `Game start,${f()}`;
//按分隔字符分为数组
str.split("a")  //a字符串进行分隔成数组






string.includes("banana");     // true
string.startsWith("apple");    // true
string.endsWith("apple");      // false
string.startsWith("banana", 6)  // true


// 重复字符串， 0  -1 取0 负数或者 Infinity报错NaN视为0，字符串会转换
console.log("Hello,".repeat(2));  // "Hello,Hello,"

// padStart：返回新的字符串，表示用参数字符串从头部（左侧）补全原字符串。
// padEnd：返回新的字符串，表示用参数字符串从尾部（右侧）补全原字符串。
console.log("h".padStart(5, "o"));  // "ooooh"
console.log("h".padEnd(5, "o"));    // "hoooo"
console.log("h".padStart(5));      // "    h"
console.log("hello".padStart(5, "A"));  // "hello"
console.log("hello".padEnd(10, ",world!"));  // "hello,worl"
console.log("123".padStart(10, "0"));  // "0000000123"

// 模板字符串,其他用法暂时还未涉及到，了解到这里
let string = `Hello'\n'world`;
console.log(string);
// "Hello'
// 'world"

let string1 = `Hey,
can you stop angry now?`;
console.log(string1);
// Hey,
// can you stop angry now?

// 变量名写在 ${} 中，${} 中可以放入 JavaScript 表达式
let name = "Mike";
let age = 27;
let info = `My Name is ${name},I am ${age + 1} years old next year.`
console.log(info);
// My Name is Mike,I am 28 years old next year.

function f() {
    return "have fun!";
}
let string2 = `Game start,${f()}`;
console.log(string2);  // Game start,have fun!




