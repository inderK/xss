// ===是完全等于，用来识别基本数据类型是可行的

let num = 5;
let str = '5';

num == str;//true
num === str;//false

'' == false;//true
'' === false;//false

null == undefined;//true
null === undefined;//false

// 类型判断
判断是否是数字
isNaN(1) && typeof (1) === 'number'
// 排除 isNaN(undefined) isNaN()

是否字符串
typeof ("") === 'string'
是否undefined
undefined === undefined   //用==无法判断null
是否null
null === null             //用==无法判断undefined
true
0 === true            //用==无法判断0
false
0 === false

判断是否是对象
obj.toString() === '[object Object]'
Object.prototype.toString.call(obj) === '[object Object]'
// typeof {}	               'object'
// typeof []	               'object'
// typeof null	               'object'
