ES2015(ES6) 新增加了两个重要的 JavaScript 关键字: let 和 const。
let 声明的变量只在 let 命令所在的代码块内有效。

const 声明一个只读的常量，一旦声明，常量的值就不能改变。

const对值的声明比较严格
比如
const a =1
a=2   就会报错
//Uncaught TypeError: Assignment to constant variable.
    at <anonymous>:1:2

const声明的常量必须初始化，而let声明的变量不用
const 的本质: const 定义的变量并非常量，并非不可变，它定义了一个常量引用一个值。使用 const 定义的对象或者数组，其实是可变的。

但这种并不推荐使用，可以修改，包括增删改，但不能直接重新赋值


let和const都是块级作用域生效，如果到函数块以外就会出现未定义，这里涉及到变量的提升
var可以声明多次，而其他不行，它也不会变量提升
ES6 明确规定，代码块内如果存在 let 或者 const，代码块会对这些命令声明的变量从块的开始就形成一个封闭作用域。代码块内，在声明变量 PI 之前使用它会报错。

const 如何做到变量在声明初始化之后不允许改变的？其实 const 其实保证的不是变量的值不变，而是保证变量指向的内存地址所保存的数据不允许改动。此时，你可能已经想到，简单类型和复合类型保存值的方式是不同的。是的，对于简单类型（数值 number、字符串 string 、布尔值 boolean）,值就保存在变量指向的那个内存地址，因此 const 声明的简单类型变量等同于常量。而复杂类型（对象 object，数组 array，函数 function），变量指向的内存地址其实是保存了一个指向实际数据的指针，所以 const 只能保证指针是固定的，至于指针指向的数据结构变不变就无法控制了，所以使用 const 声明复杂类型对象时要慎重。

待续..............................

变量的提升在webpack里面看编译后的代码就可以很明显的用到
首先只有变量本身的声明会被提升，赋值和其他运算会留在原地
函数的声明也会被提升，，函数调用留在原地
函数和变量提升优先函数，然后是变量声明，但这种情况下通过变量值来进行函数运行是不可靠的比如
foo(); // "b"
var a = true;
if (a) {
  function foo() { console.log("a"); }
}
else {
  function foo() { console.log("b"); }
}

如果变量声明使用let，那么这个声明不会在块作用域上提升，而是按顺序声明赋值
比如循环里面使用let i = 0 ，那么每一个i都在for的一个循环的块里面，是不一样的值
for (let i = 0; i < 10; i++) {
  console.log(i);
}
console.log(i); // ReferenceError



const 声明创建一个值的只读引用 (即指针)，
这里就要介绍下 JS 常用类型: String、Number、Boolean、Array、Object、Null、Undefined。
其中基本类型有 Undefined、Null、Boolean、Number、String，保存在栈中；
复合类型 有 Array、Object ，保存在堆中； 
基本数据当值发生改变时，那么其对应的指针也将发生改变，故造成 const申明基本数据类型时，再将其值改变时，将会造成报错， 
例如 const a = 3 ; a = 5 时 将会报错；但是如果是复合类型时，如果只改变复合类型的其中某个Value项时， 将还是正常使用

注意，解构相当于浅拷贝，只会拷贝一层引用对象