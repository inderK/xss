遍历对象
let obj = {
    'a': 'aa',
    'b': 'bb',
    'c': 'cc',
    'd': 'dd'
};
for (let i in obj) { console.log(i, obj[i]); }

Object.prototype.c = "111111"; // 在原型链上增加一个可枚举属性
var propertyArr = Object.keys(obj);
for (let unit of propertyArr) {
    console.log(unit, obj[unit]);
}



合并对象



判断对象相等



查找对象属性
obj["a"]  //打印的是值
for (let i in obj) { console.log(i, obj[i]); }


对象拷贝方法