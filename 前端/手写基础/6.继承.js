//ES5 继承（寄生组合继承）
// function Parent(name) {
//     this.name = name
// }
// Parent.prototype.eat = function () {
//     console.log(this.name + " is eating")
// }
// function Child(name, age) {
//     Parent.call(this, name)
//     this.age = age
// }
// console.log(Parent.prototype)
// Child.prototype = Object.create(Parent.prototype)
// Child.prototype.contructir = Child

// let xm = new Child("xiaoming", 12)

// console.log(xm.name)
// console.log(xm.age)
// xm.eat()


//ES6继承
class Parent {
    constructor(name) {
        this.name = name
    }
    eat() {
        console.log(this.name + " is eating")
    }
}

class Child extends Parent {
    constructor(name, age) {
        super(name)
        this.age = age
    }
}

let xl = new Child("xiaoli", 12)
console.log(xl.name)
console.log(xl.age)
xl.eat()
