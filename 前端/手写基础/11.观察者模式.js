// 目标和观察者是基类，目标提供维护观察者的一系列方法，观察者提供更新接口。具体观察者和具体目标继承各自的基类，然后具体观察者把自己注册到具体目标里，在具体目标发生变化时候，调度观察者的更新方法。

class Subject {//目标类===被观察者
    constructor() {
        this.subjectList = [];//目标列表
    }
    add(fn) {//注册
        this.subjectList.push(fn)
    }
    notify(context) {//发通知    
        var subjectCount = this.subjectList.length
        for (var i = 0; i < subjectCount; i++) {
            this.subjectList[i].update(context)
        }
    }
    //取消注册
    remove(fn) {
        this.subjectList.splice(this.subjectList.indexOf(fn), 1)
    }
}

class Observer {//观察者类==观察者
    update(data) {
        console.log('updata +' + data)
    }
}

var Subject1 = new Subject()//具体目标1
var Subject2 = new Subject()//具体目标2

var Observer1 = new Observer()//具体观察者1
var Observer2 = new Observer()//具体观察者2

Subject1.add(Observer1);//注册 //updata +test1
Subject1.add(Observer2);//注册 //updata +test1
Subject2.add(Observer1);//注册 //updata +test2

Subject1.notify('test1')//发布事件
Subject2.notify('test2')//发布事件

// 从上面代码可以看出来，先创建具体目标和具体观察者，然后通过add方法把具体观察者 Observer1、Observer2注册到具体目标中，目标和观察者是直接联系起来的，所以具体观察者需要提供update方法。在Subject1中发通知时，Observer1、Observer2都会接收通知从而更改状态。