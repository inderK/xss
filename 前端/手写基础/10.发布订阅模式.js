//这个再pdfjs源码里面出现过，类似于注册点击事件
// Subscribe/Publish模式使用了一个主题/事件通道，这个通道介于订阅者和发布者之间。该事件系统允许代码定义应用程序的特定事件，该事件可以传递自定义参数，自定义参数包含订阅者所需要的值。其目的是避免订阅者和发布者产生依赖关系。
// 观察者模式存在一个问题，目标无法选择自己想要的消息发布，观察者会接收所有消息。在此基础上，出现了发布/订阅模式，在目标和观察者之间增加一个调度中心。订阅者（观察者）把自己想订阅的事件注册到调度中心，当该事件触发时候，发布者（目标）发布该事件到调度中心，由调度中心统一调度订阅者注册到调度中心的处理代码。
class EventEmitter {
    constructor() {
        this.cache = {}
    }
    on(name, fn) {
        if (this.cache[name]) {
            this.cache[name].push(fn)
        } else { this.cache[name] = [fn] }
    }
    off(name, fn) {
        const tasks = this.cache[name]
        if (tasks) {
            const index = tasks.findIndex((f) => { return f === fn || f.callback === fn })
            if (index >= 0) { tasks.splice(index, 1) }
        }
    }
    emit(name, once = false) {
        if (this.cache[name]) {
            //创建副本，如果回调函数内继续注册相同事件，会造成死循环
            const tasks = this.cache[name].slice()
            for (let fn of tasks) { fn() }
            if (once) { delete this.cache[name] }
        }
    }
}


const enentBus = new EventEmitter()
const tasks1 = () => { console.log("task1") }
const tasks2 = () => { console.log("tasks2") }

enentBus.on("task", tasks1)
enentBus.on("task2", tasks2)
// enentBus.off("task", tasks1)

setTimeout(() => { enentBus.emit("task") }, 1000)  //tasks2
// 可以看出来，订阅/发布模式下：订阅和发布是不直接调度的，而是通过调度中心来完成的，订阅者和发布者是互相不知道对方的，完全不存在耦合。