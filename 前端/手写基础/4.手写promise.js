class MyPromis {
    constructor(executor) {
        this.status = "pending";//初始状态为等待
        this.value = null;//成功的值
        this.reason = null;//失败的原因
        this.onFulfilledCallbacks = [];//成功的回调函数数组
        this.onRejectefCallbacks = [];//失败的回调函数数组
        let resolve = value => {
            if (this.status == "pending") {
                this.status = "fulfilled";
                this.value = value;
                this.onFulfilledCallbacks.forEach(fn => fn())
            }
        }
        let reject = reason => {
            if (this.status == "pending") {
                this.status = "rejected";
                this.reason = reason;
                this.onRejectefCallbacks.forEach(fn => fn())
            }
        }
        try {
            console.log(executor)
            executor(resolve, reject)
        } catch (error) {
            reject(error)
        }
    }
    then(onFulfilled, onRejected) {
        return new MyPromis((resolve, reject) => {
            if (this.status == "fulfilled") {
                setTimeout(() => {
                    const x = onFulfilled(this.value);
                    x instanceof MyPromis ? x.then(resolve, reject) : resolve(x)
                })
            }
            if (this.status == "rejected") {
                setTimeout(() => {
                    const x = onRejected(this.reason)
                    x instanceof MyPromis ? x.then(resolve, reject) : resolve(x)
                })
            }

            if (this.status == "pending") {
                this.onFulfilledCallbacks.push(() => {
                    setTimeout(() => {
                        const x = onFulfilled(this.value)
                        x instanceof MyPromis ? x.then(resolve, reject) : resolve(x)
                    })
                })
                this.onRejectefCallbacks.push(() => {
                    setTimeout(() => {
                        const x = onRejected(this.reason)
                        x instanceof MyPromis ? x.then(resolve, reject) : resolve(x)
                    })
                })
            }
        })
    }
}


function p1() {
    return new MyPromis((resolve, reject) => {
        setTimeout(resolve, 1000, 1)
    })
}

function p2() {
    return new MyPromis((resolve, reject) => {
        setTimeout(resolve, 1000, 2)
    })
}




function promise() {
    console.log("sssF")
    p1().then(res => {
        console.log(res)
        return p2()
    }).then(ret => {
        console.log(ret)
    })



}



















