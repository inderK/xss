
window.onload = () => {
    function debounce(fn, delay = 500) {
        let timer
        return function () {
            console.log(timer)
            if (timer) { clearTimeout(timer) }
            //这个时候就需要绑定到this，因为箭头函数会让this绑定到setTimeout上
            timer = setTimeout(() => { fn.apply(this) }, delay)
            //这样的写法就不会让this指向其他地方
            // timer = setTimeout(fn, delay)
        }
    }
    let input = document.getElementById("防抖按钮绑定点击事件")
    input.addEventListener('click', debounce(() => { console.log("防抖按钮绑定点击事件") }, 1000))


    function throttle(fn, delay) {
        let timer = null
        return function () {
            let context = this;
            let args = arguments;
            //可以有两个逻辑，一个是timer存在就return，一个是timer不存在就设置定时器，存在反而什么都不做
            if (timer) return
            // if (!timer) {
                timer = setTimeout(function () {
                    fn.apply(context, args);
                    timer = null;
                }, delay);
            // }
        }
    }
    let input2 = document.getElementById("节流按钮绑定点击事件")
    input2.addEventListener('click', throttle(() => { console.log("节流按钮绑定点击事件") }, 1000))



}