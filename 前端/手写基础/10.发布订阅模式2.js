class Public {//事件通道
    constructor() {
        this.handlers = {};
    }
    on(eventType, handler) { // 订阅事件
        var self = this;
        if (!(eventType in self.handlers)) {
            self.handlers[eventType] = [];
        }
        self.handlers[eventType].push(handler);
        return self;
    }
    emit(eventType) {    // 发布事件
        var self = this;
        var handlerArgs = Array.prototype.slice.call(arguments, 1);
        var length = self.handlers[eventType].length
        for (var i = 0; i < length; i++) {
            self.handlers[eventType][i].apply(self, handlerArgs);
        }
        return self;
    }
    off(eventType, handler) {    // 删除订阅事件
        var currentEvent = this.handlers[eventType];
        var len = 0;
        if (currentEvent) {
            len = currentEvent.length;
            for (var i = len - 1; i >= 0; i--) {
                if (currentEvent[i] === handler) {
                    currentEvent.splice(i, 1);
                }
            }
        }
        return self;
    }
}

//订阅者
function Observer1(data) {
    console.log('订阅者1订阅了:' + data)
}
function Observer2(data) {
    console.log('订阅者2订阅了:' + data)
}

var publisher = new Public();

//订阅事件
publisher.on('a', Observer1);
publisher.on('b', Observer1);
publisher.on('a', Observer2);

//发布事件
publisher.emit('a', '第一次发布的a事件');
publisher.emit('b', '第一次发布的b事件');
publisher.emit('a', '第二次发布的a事件');
//订阅者1订阅了:第一次发布a事件
//订阅者2订阅了:第一次发布a事件
//订阅者1订阅了:第一次发布b事件
//订阅者1订阅了:第二次发布a事件
//订阅者2订阅了:第二次发布a事件
