let number = 8

function res1(num) {
    if (num == 0) { return 0 }
    if (num == 1) { return 1 }
    return res1(num - 1) + res1(num - 2)
}
console.log(res1(number))
//这种算法会在n>100的时候卡死

//优化以后，还可以继续优化，使用双指针暂存数据
function minCost(cost) {
    if (cost.length < 2) return 0;
    let list = [0, 0];
    for (let i = 2; i <= cost.length; i++) {
        list[i] = Math.min(list[i - 1] + cost[i - 1], list[i - 2] + cost[i - 2]);
    }
    return list[cost.length];
}
