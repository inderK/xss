// 现在有一种密码变换算法。
// 九键手机键盘上的数字与字母的对应： 1--1， abc--2, def--3, ghi--4, jkl--5, mno--6, pqrs--7, tuv--8 wxyz--9, 0--0，把密码中出现的小写字母都变成九键键盘对应的数字，如：a 变成 2，x 变成 9.
// 而密码中出现的大写字母则变成小写之后往后移一位，如：X ，先变成小写，再往后移一位，变成了 y ，例外：Z 往后移是 a 。
// 数字和其它的符号都不做变换。
// 数据范围： 输入的字符串长度满足 1 \le n \le 100 \1≤n≤100 


YUANzhi1987
zvbo9441987


const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let str = "";
    while ((line = await readline())) {
        str = line;
    }

    const realPassword = [...str].map((char) => {
        switch (true) {
            case /[A-Y]/.test(char):
                return String.fromCharCode(char.charCodeAt() + 1).toLowerCase();
            case /Z/.test(char):
                return "a";
            case /[a-z]/.test(char):
                switch (true) {
                    case /[abc]/.test(char):
                        return 2;
                    case /[def]/.test(char):
                        return 3;
                    case /[ghi]/.test(char):
                        return 4;
                    case /[jkl]/.test(char):
                        return 5;
                    case /[mno]/.test(char):
                        return 6;
                    case /[pqrs]/.test(char):
                        return 7;
                    case /[tuv]/.test(char):
                        return 8;
                    case /[wxyz]/.test(char):
                        return 9;
                }
            case /[0-9]/.test(char):
                return char;
        }
    });

    console.log(realPassword.join(""));
})();
