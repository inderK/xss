// 明明生成了NN个1到500之间的随机整数。请你删去其中重复的数字，即相同的数字只保留一个，把其余相同的数去掉，然后再把这些数从小到大排序，按照排好的顺序输出。






const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let res = [];
    while ((line = await readline())) {
        res.push(line);
    }
    let num = res.shift();
    //去重
    let myOrderedArray = res.reduce(function (accumulator, currentValue) {
        if (accumulator.indexOf(currentValue) === -1) {
            accumulator.push(currentValue);
        }
        return accumulator;
    }, []);
    //排序输出
    myOrderedArray.sort((a, b) => a - b).forEach((item) => console.log(item));
})();
