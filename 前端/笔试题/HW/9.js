// 输入一个 int 型整数，按照从右向左的阅读顺序，返回一个不含重复数字的新的整数。
// 保证输入的整数最后一位不是 0 。


// 输入：
// 9876673
// 输出：
// 37689


const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    while ((line = await readline())) {
        let res = String(line);
        res = res.split("");
        let num = res.length;
        let list = [];
        while (num--) {
            !list.includes(res[num ]) &&  list.push(res[num ]);
        }
        console.log(list.join(""))
    }
})();
