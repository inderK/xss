// 计算字符串最后一个单词的长度，单词以空格隔开，字符串长度小于5000。（注：字符串末尾不以空格为结尾）
// 输入描述：
// 输入一行，代表要计算的字符串，非空，长度小于5000。

// 输出描述：
// 输出一个整数，表示输入字符串最后一个单词的长度。
const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let res = [];
    while ((line = await readline())) {
        res = line;
    }
    let right = res.match(/(?<=\s)(?=\S+)\w*/gim);
    if (right) {
        console.log(right[right.length - 1].length);
    } else {
        console.log(res.length)
    }
})();

//正则方法
//匹配到最后一个以空格开始的字符串（在这个位置之前需要匹配到至少一个空格，在这个位置之后匹配到没有空格的字符串）  (?<=\s)(?=\S)\w*
/**
 (?=pattern) 零宽正向预测先行断言  代表字符串中的一个位置，紧接该位置之后的字符序列能够匹配pattern
 (?!pattern) 零宽负向预测先行断言  代表字符串中的一个位置，紧接该位置之后的字符序列不能匹配pattern
 (?<=pattern) 零宽正向预测后行断言  代表字符串中的一个位置，紧接该位置之前的字符序列能够匹配pattern
 (?<!pattern) 零宽负向预测后行断言  代表字符串中的一个位置，紧接该位置之前的字符序列不能匹配pattern
 */


// const rl = require("readline").createInterface({ input: process.stdin });
// var iter = rl[Symbol.asyncIterator]();
// const readline = async () => (await iter.next()).value;

// void (async function () {
//     // Write your code here
//     let res = [];
//     while ((line = await readline())) {
//         res = line;
//     }
//     let list = res.split(" ");
//     res = list[list.length - 1];
//     let nums = 0;
//     for (let i in res) {
//         if (i) {
//             nums++;
//         } else {
//             return;
//         }
//     }
//     console.log(nums);


// })();
