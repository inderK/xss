// 功能:输入一个正整数，按照从小到大的顺序输出它的所有质因子（重复的也要列举）（如180的质因子为2 2 3 3 5 ）


const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    while ((line = await readline())) {
        let num = parseInt(line);
        let num_sqrt = Math.sqrt(num);
        const array = [];

        for (let i = 2; i <= num_sqrt; i++) {
            while (num % i === 0) {
                array.push(i);
                num /= i;
            }
        }
        if (num != 1) {
            array.push(num);
        }

        console.log(array.join(" "));
    }
})();
