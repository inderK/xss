const rl = require("readline").createInterface({ input: process.stdin });

// 密码要求:

// 1.长度超过8位

// 2.包括大小写字母.数字.其它符号,以上四种至少三种

// 3.不能有长度大于2的包含公共元素的子串重复 （注：其他符号不含空格或换行）

// 数据范围：输入的字符串长度满足 1 \le n \le 100 \1≤n≤100 

var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let res = [];
    while ((line = await readline())) {
        res.push(line);
    }

    function tes(line) {
        const regcap = /[A-Z]+/;
        const regchar = /[a-z]+/;
        const regnum = /[0-9]+/;
        const other = /[^A-Za-z0-9]+/;
        let regarr = [regcap, regchar, regnum, other];
        let condition1 = line.length - 8 > 0;
        let condition2 =
            regarr
                .map((item) => item.test(line))
                .reduce((pre, cur) => pre + cur) >= 3;
        let condition3 = ((str) => {
            for (let i = 0; i < str.length - 2; i++) {
                let substr = str.slice(i, i + 3);
                if (str.indexOf(substr) != str.lastIndexOf(substr)) {
                    return false;
                }
            }
            return true;
        })(line);
        return condition1 && condition2 && condition3
            ? console.log("OK")
            : console.log("NG");
    }
    res.forEach((item) => tes(item));
})();
