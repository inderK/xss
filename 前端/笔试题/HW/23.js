// 输出描述：
// 删除字符串中出现次数最少的字符后的字符串。
// 输入：
// aabcddd
// 输出：
// aaddd

const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    while ((line = await readline())) {
        let obj = {};
        Array.from(line).forEach((item) => {
            if (obj[item]) {
                obj[item]++;
            } else {
                obj[item] = 1;
            }
        });
        let res = [];
        let number = line.length;
        Object.keys(obj).forEach((item) => {
            if (obj[item] < number) number = obj[item];
        });
        Object.keys(obj).forEach((item) => {
            if (obj[item] == number) {
                res.push(item);
            }
        });
        let R = "";
        Array.from(line).forEach((item) => {
            if (!res.includes(item)) {
                R = R + item;
            }
        });
        console.log(R);
    }
})();
