// 输入描述：
// 第一行输入一个由字母和数字以及空格组成的字符串，第二行输入一个字符。

// 输出描述：
// 输出输入字符串中含有该字符的个数。（不区分大小写字母）


const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let list = [];
    while ((line = await readline())) {
        list.push(line);
    }
    // list = ["8 8 8  8A i i OOI              IIIaa", "A"];
    let reg = new RegExp(list[1], "img");
    console.log(list[0].match(reg)?.length || 0);
})();
