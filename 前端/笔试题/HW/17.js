// 开发一个坐标计算工具， A表示向左移动，D表示向右移动，W表示向上移动，S表示向下移动。从（0,0）点开始移动，从输入字符串里面读取一些坐标，并将最终输入结果输出到输出文件里面。

// 输入：

// 合法坐标为A(或者D或者W或者S) + 数字（两位以内）

// 坐标之间以;分隔。

// 非法坐标点需要进行丢弃。如AA10;  A1A;  $%$;  YAD; 等。

const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let list = [];
    while ((line = await readline())) {
        list = line.split(";");
    }
    
    //筛选出有效,正则特别好用，匹配大写WSAD和数字结尾即可
    let pattern = /^(W|S|A|D)\d{1,}$/;
    let reslist = list.filter((item) => { return pattern.test(item) });
    let res = [0, 0];
    //创建移动规则
    let rule = {
        W: (location, long) => {
            location[1] = location[1] + long;
        },
        S: (location, long) => {
            location[1] = location[1] - long;
        },
        A: (location, long) => {
            location[0] = location[0] - long;
        },
        D: (location, long) => {
            location[0] = location[0] + long;
        },
    };
    //对筛选后的数组动作进行遍历，对移动规则进行匹配，匹配到的进行移动
    reslist.forEach((item) => {
        Object.keys(rule).forEach((item0) => {
            if (item.includes(item0)) {
                rule[item0](res, Number(item.substr(1)));
            }
        });
    });
    console.log(res.toString());
})();

