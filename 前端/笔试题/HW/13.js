// 输入：
// I am a boy
// 输出：
// boy a am I

const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void async function () {
    // Write your code here
   while ((line = await readline())) {
        let list = line.split(" ");
        let reslist = [];
        list.forEach(item => { reslist.unshift(item); })
        console.log(reslist.join(" "));
    }
}()
