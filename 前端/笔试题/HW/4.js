// •输入一个字符串，请按长度为8拆分每个输入字符串并进行输出；

// •长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。




const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    while ((line = await readline())) {
        let num = line.length % 8;
        if (num !== 0) {
            num = 8 - num;
            while (num--) {
                line = line + "0";
            }
        }
        num = line.length / 8;
        let i = 0;
        while (num--) {
            console.log(line.slice(8 * i, 8 * (i + 1)));
            i++;
        }
    }
})();
