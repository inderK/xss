合并表记录
// 输入：
// 4
// 0 1
// 0 2
// 1 2
// 3 4
// 输出：
// 0 3
// 1 2
// 3 4

const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let count;
    let list = [];
    while ((line = await readline())) {
        let tokens = line.split(" ");
        if (tokens.length == 1) {
            count = tokens;
        } else {
            list.push(tokens);
        }
    }
    let tmp = [];
    list.forEach((item) => {
        const [index, value] = item;
        if (tmp[index]) {
            tmp[index] = tmp[index] + Number(value);
        } else {
            tmp[index] = Number(value);
        }
    });
    tmp.forEach((item, index) => {
        item && console.log(index, item);
    });
})();