// 输入：
// 9
// cap
// to
// cat
// card
// two
// too
// up
// boat
// boot
// 复制
// 输出：
// boat
// boot
// cap
// card
// cat
// to
// too
// two
// up

const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    let list = [];
    while ((line = await readline())) {
        list.push(line);
    }
    list.shift();
    list.sort().forEach((item) => {
        console.log(item);
    });
})();
