// 输入一个 int 型的正整数，计算出该 int 型数据在内存中存储时 1 的个数。

// 数据范围：保证在 32 位整型数字范围内
// 输入：
// 5
// 复制
// 输出：
// 2

const rl = require("readline").createInterface({ input: process.stdin });
var iter = rl[Symbol.asyncIterator]();
const readline = async () => (await iter.next()).value;

void (async function () {
    // Write your code here
    while ((line = await readline())) {
        let str = Number(line).toString(2);
        let resnum = 0;
        for (let i in str) {
            if (str[i] === "1") {
                resnum++;
            }
        }
        console.log(resnum);
    }
})();
