let nodes = [
    {
        id: "1",
        label: "1",
        children: undefined
    },
    {
        id: "2",
        label: "2",
        children: [
            {
                id: "21",
                label: "21",
                children: undefined
            },
        ]
    },
    {
        id: "3",
        label: "3",
        children: undefined
    },
]

function searchTree(nodes = [], searchKey = "") {
    let res = null

    let loop = (list) => {
        list.forEach(item => {
            if (item.label == searchKey) {
                res = item.id
            }
            if (item.children && Array.isArray(item.children)) {
                loop(item.children)
            }
        })
    }
    loop(nodes)
    return res
}

console.log(searchTree(nodes, "121"))