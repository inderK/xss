class Stack {
    constructor() {
        this.list = []
    }
    in(value) {
        // 数据进栈
        // todo: 你的代码
        this.list.push(value)
    }
    out() {
        // 数据出栈
        // todo: 你的代码
        console.log(this.list.pop())
    }
    top() {

        // 返回栈顶的数据
        // todo: 你的代码
        console.log(this.list[this.list.length - 1])
    }
    size() {
        // 返回栈数据的长度
        // todo: 你的代码
        console.log(this.list.length)
    }
}

// 要求当执行下列代码时，能输出预期的结果,需要输出的话就需要加上 console.log
const stack = new Stack()
stack.in('x')
stack.in('y')
stack.in('z')

stack.top() // 输出 'z'
stack.size() // 输出 3

stack.out() // 输出 'z'
stack.top() // 输出 'y'
stack.size() // 输出 2