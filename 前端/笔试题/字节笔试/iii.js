/**
  eg：input: [[5, 7, 4], 3, [89, [8, 9]]]
      ouput: [5, 7, 4, 3, 89, 8, 9] 
**/

// 不可以使用 Array.prototype.flat() 来实现

function flat(arr = []) {

    const res = arr.reduce(function (pre, curr) {
        if (Array.isArray(curr)) {
            curr.forEach(item => {
                arguments.callee(pre, item)
            })
        } else {
            pre.push(curr)
        }
        return pre
    }, [])
    return res
}


let list = [[5, 7, 4], 3, [89, [8, 9]]]
console.log(flat(list))