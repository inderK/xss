this.age=5
function A() {
    let person = {
        name: "张三",
        age: 10,
        getName: function () {
            console.log(this.name);
        },
        getAge: () => {
            console.log(this.age);
        }
    }
    return person;
}
function B() {
    this.name = "李四";
    this.age = 20;
}

// 以下每一行代码执行，会输出什么内容
A().getName()
A().getAge()
B()
A().getName()
A().getAge()