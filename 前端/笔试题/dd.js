
function render(template, data) {
    return template.replace(/{{(.*?)}}/g, function (match, group) {
        const content = group.trim();
        return data[content];
    })
}

const template = `<div>{{ name }}<span>{{ age }}</span><div>`;
const data = { name: 'singh', age: 27 };

console.log(render(template, data))
// <div>singh<span>27</span><div>


function name(trm, data) {
    return template.replace(/{{(.*?)}}/, function name(matchg, group) {
        const d = group.trim()
        return data[d]
    })
}


const isPromise = function (promise) {
    return (
        !!promise && (
            (typeof promise === "object" || typeof promise === "function")
            && (
                typeof promise.then === "function"
            )

        )
    )
}

//实现promiseall
const p1 = new Promise((res, rej) => {
    setTimeout(() => {
        res("1")
    }, 100);
})
const p2 = new Promise((res, rej) => {
    setTimeout(() => {
        res("2")
    }, 1000);
})
const p3 = new Promise((res, rej) => {
    setTimeout(() => {
        res("3")
    }, 10000);
})


const promiseAll = function (promises = []) {

    let results = []
    let promiseCount = 0
    let promisesLength = promises.length

    return new Promise((resolve, reject) => {

        promises.forEach((item, index) => {
            Promise.resolve(val).then((res) => {
                promiseCount++
                results[index] = res
                if (promisesLength == promiseCount) {
                    resolve(results)
                }
            }, (err) => {
                return reject(err)
            }

            )
        })
    })

}



//定时器做防抖
function debounce(callBack, time) {

    let timer = null
    function _debounce(...params) {
        if (!!timer) {
            clearTimeout(timer)
        } else {
            timer = setTimeout(() => {
                callBack.apply(this, params)
                timer = null
            }, time);
        }
    }
    return _debounce

}

// debounce((params) => { console.log(1), 100 })



function sleep(delay) {
    return new Promise(res => {
        return setTimeout(res, delay);
    })
}




const { a: q, b: w } = { a: 1, b: 2 }
console.log(q.w)