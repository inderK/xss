import React from 'react';
import './0demo.css'

const demo = () => {
    //原始方法
    function ajax(url, cb) {
        let xhr = new XMLHttpRequest()
        xhr.open("GET", "http://localhost:3001", true);
        // 建立请求，最终会调用cb(..) 
        //下面两个都可以
        // xhr.onreadystatechange = cb.bind(this, xhr)
        xhr.onreadystatechange = () => cb(xhr)

        xhr.send();
        // onabort: null
        // onerror: null
        // onload: null
        // onloadend: null
        // onloadstart: null
        // onprogress: null
        // onreadystatechange: null
        // ontimeout: null
        // readyState: 4
        // response: "<h1>Hello World!</h1>"
        // responseText: "<h1>Hello World!</h1>"
        // responseType: ""
        // responseURL: "http://localhost:3001/"
        // responseXML: null
        // status: 200
        // statusText: "OK"
        // timeout: 0
        // upload: XMLHttpRequestUpload {onloadstart: null, onprogress: null, onabort: null, onerror: null, onload: null, …}
        // withCredentials: false
    }
    function Click() {
        ajax('http://localhost:3001', (xhr) => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                console.log(xhr)
                document.getElementById("ajax").innerHTML = xhr.responseText;
            }
        });
    }
    function reset() {
        document.getElementById("ajax").innerHTML = "重置了吧"
    }

    //实现一个生成器
    function* main1() {
        console.log("sadsasdas")
        let ret = 1;
        console.log(ret,)
        ret = yield 2;
        console.log(ret,)
        try {
            ret = yield "sdsad"
            console.log(ret,)
        }
        catch (err) {
            ret = yield Promise.resolve(err + 1);
        }
        ret = yield Promise.all([
            Promise.resolve(ret + 1),
            Promise.resolve(ret + 1),
            Promise.resolve(ret + 1)
        ]);
        yield Promise.resolve(ret + 1)
        console.log(ret, "ssd")
    }

    let res = main1()
    //运行这个生成器
    function run() {
        console.log('运行')
        res.next()
    }

    //实现一个迭代器
    let arr = [1, 2, 3]
    let it = arr[Symbol.iterator]()
    function diedai() {
        let res = it.next()
        console.log(res, arr, "sss")
    }


    function promise() {

        let pro = new Promise(function pr(resolve, reject) {
            let xhr = new XMLHttpRequest()
            xhr.open("GET", "http://localhost:3001", true);
            // 建立请求，最终会调用cb(..) 
            //下面两个都可以
            // xhr.onreadystatechange = cb.bind(this, xhr)
            xhr.onreadystatechange = () => resolve(xhr)

            xhr.send();
        })
        pro = Promise.resolve("拦截")
        pro.then(
            res => {
                console.log(res)
                document.getElementById("promise").innerHTML = 'res'
                return "成功"
            },
            rej => {
                console.log(rej)
                document.getElementById("promise").innerHTML = 'rej'
                return "失败"
            }

        ).catch(err => {
            console.log(err)
        })


        let p1 = Promise.resolve("p1")
        let p2 = new Promise(res => {
            res("p2")
        })
        Promise.all([p1, p2]).then(() => {

            console.log(p1, p2, "完成")
        })




    }

    function reset_promise() {
        document.getElementById("promise").innerHTML = "重置了吧"
    }

    return <div className='demo'>
        <div className='sd'>

            <div className='item'>1</div>
            <div className='item'>2</div>
            <div className='item'>3</div>
            <div className='item'>4</div>
            <div></div>
            <div className='item'>5</div>
            <div className='item'>6</div>
            <div className='item'>7</div>
            <div></div>
            <div></div>
            <div className='item'>8</div>
            <div className='item'>9</div>
            <div></div>
            <div></div>
            <div></div>
            <div className='item'>10</div>
        </div>

        <div className='content'>

            <div className='item0'>左边</div>
            <div className='item0'>

                <div className='top' >中间</div>
                <div>中间下面</div>
            </div>
            <div className='item0'>

                <div className='righttop'>右边上面</div>
                <div>右边下面</div>
            </div>
        </div>
    </div>
}
export default demo
