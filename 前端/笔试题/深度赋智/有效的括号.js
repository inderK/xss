/**
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。

有效字符串需满足：

左括号必须用相同类型的右括号闭合。
左括号必须以正确的顺序闭合。
每个右括号都有一个对应的相同类型的左括号。

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/valid-parentheses
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
    if (s.length == 0) return false
    if (s.length % 2 == 1) return false
    let res = s.split("")
    if ([...new Set(res)].length % 2 == 1) return false
    let re = true
    let list = []
    //返回在数组的下标
    const ind = (str, list) => {
        let res = []
        list.forEach((item, index) => {
            if (item == str) {
                res.push(index)
            }
        })
        return res
    }

    res.forEach((item, index) => {

        //在后面%2==1的地方有就行
        if (item == "(") {
            if (!ind(")", res).some((item, i) => { return (item - index) % 2 == 1 })) {
                re = false
            }
        }
        if (item == "[") {
            if (!ind("]", res).some((item, i) => { return (item - index) % 2 == 1 })) {
                re = false
            }


        }
        if (item == "{") {
            if (!ind("}", res).some((item, i) => { return (item - index) % 2 == 1 })) {
                re = false
            }
        }
    });




    return re
};
console.log(

    isValid("({{{{}}}))")
)



/**
 * 进出栈
 */

const map = {
    '{': '}',
    "[": ']',
    "(": ")"
}
const match = (str) => {
    const splitList = str.split('');
    const matchList = [];
    for (let i = 0; i < splitList.length; i++) {
        const strItem = splitList[i];
        if (map[strItem]) {
            matchList.push(strItem);
        } else {
            const lastItem = matchList.pop();
            if (map[lastItem] !== strItem) {
                return false;
            }
        }

    }
    return matchList.length == 0;
}

console.log(match("()"))
console.log(match("({})"))
console.log(match("(})"))
console.log(match("("))


/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
    let reg = /(\{\})|(\(\))|(\[\])/g
    while (s.match(reg)) { s = s.replace(reg, '') }
    return s.length == 0
};

// 作者：crazyyoung
// 链接：https://leetcode.cn/problems/valid-parentheses/solution/san-xing-dai-ma-javascriptzheng-ze-pi-pe-7u83/
// 来源：力扣（LeetCode）
// 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。