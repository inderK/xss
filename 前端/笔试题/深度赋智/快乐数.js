
/**
 * 编写一个算法来判断一个数 n 是不是快乐数。

「快乐数」 定义为：

对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和。
然后重复这个过程直到这个数变为 1，也可能是 无限循环 但始终变不到 1。
如果这个过程 结果为 1，那么这个数就是快乐数。
如果 n 是 快乐数 就返回 true ；不是，则返回 false 。

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/happy-number
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

//
/**
 * @param {number} n
 * @return {boolean}
 */
var isHappy = function (n) {
    if (n == 1) return true
    const getNum = (num) => {
        num = num.toString()
        let list = num.split("")
        let res = list.reduce((pre, curr) => {
            pre = pre + curr * curr
            return pre
        }, 0)
        return res
    }
    let list = []
    let res
    const TF = (number) => {

        if (number == 1) return res = true
        if (list.includes(number)) return res = false
        list.push(number)
        TF(getNum(number))
    }
    TF(n)
    return res


    // const TF = (number) => {

    //     if (number == 1) return true
    //     if (list.includes(number)) return false
    //     list.push(number)
    //     return TF(getNum(number))
    // }
    // return TF(n)

};