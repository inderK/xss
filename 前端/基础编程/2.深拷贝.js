() => {
    // let a = [{ a: 1 }, { b: 2 }, { c: 3 }]
    // let b = a
    // 赋值情况下，b的指针指向a，a指向内存，两个都是同样指向内存
    // 如果通过b的赋值改变了值，那么等于改变了内存里面的值，所以a也会跟着改变
    // b[0] = 1
    // a[1, {… }, {… }]
    // b[1, {… }, {… }]
    // 对象也是如此
    // let a = { a: 1 }
    // let b = a
    // b.a = 2
    // b //{a:2}
    // a //{a:2}
}



// 如果是浅拷贝，举个例子
var obj = { a: 1, arr: [2, 3] };
var shallowObj = shallowCopy(obj);

function shallowCopy(src) {
    var dst = {};
    for (var prop in src) {
        if (src.hasOwnProperty(prop)) {
            dst[prop] = src[prop];
        }
    }
    return dst;
}

shallowObj.arr[1] = 5;
obj.arr[1]   // = 5
// 如果只遍历一层，那么下一层的对象仍然在内存区域，如果新的变量改变了内存区域的值，那么原来的变量也会跟着改变

// 深拷贝需要把所有的变量都复制过来，这里可以使用lodash 的函数
const 新对象 = _.cloneDeep(源对象)
// 或者使用手写的递归函数
export function deepClone(obj) {  //可传入对象 或 数组
    //  判断是否为 null 或 undefined 直接返回该值即可,
    if (obj === null || !obj) return obj;
    // 判断 是要深拷贝 对象 还是 数组
    if (Object.prototype.toString.call(obj) === "[object Object]") { //对象字符串化的值会为 "[object Object]"
        let target = {}; //生成新的一个对象
        const keys = Object.keys(obj); //取出对象所有的key属性 返回数组 keys = [ ]
        //遍历复制值, 可用 for 循环代替性能较好
        keys.forEach(key => {
            if (obj[key] && typeof obj[key] === "object")
                //如果遇到的值又是 引用类型的 [ ] {} ,得继续深拷贝
                target[key] = deepClone(obj[key]);//递归
            else
                target[key] = obj[key];

        })
        return target  //返回新的对象
    } else if (Array.isArray(obj)) {
        // 数组同理
        let arr = [];
        obj.forEach((item, index) => {
            if (item && typeof item === "object")
                arr[index] = deepClone(item);
            else
                arr[index] = item;
        })
        return arr
    }
}

用法
const obj = {};
const obj2 = deepClone(obj);


// 一般使用
const source = { name: '小明', sayName: () => { }, prop: undefined };
//先字符串序列化 再 parse解析回来
console.log(JSON.parse(JSON.stringify(source)))
// VM575: 1      { "name": "小明" }// 输出结果
// 该方法简单,但是会把引用类型和undefined的属性去除，函数和循环引用的情况也不适用

如果使用ES6的扩展运算
//同样代码
const sourceObj = { name: '小明', sayName: () => { }, prop: undefined, obj: {} };
const newObj = { ...sourceObj }
//先字符串序列化 再 parse解析回来
console.log(newObj)
// VM575: 1      { name: "小明", prop: undefined, sayName: () => { }, obj: { } }// 输出结果
// 对二层结构的obj 赋值
source.obj.name = 'rick';
console.log(newObj.obj.name)    //  newObj.obj.name => rick 
// 其实这样违背了深拷贝的初衷



//   考虑一种情况就是可能会有循环引用
let o = {
    a: 1
}
o.b = o
// 这个时候就需要使用消息通道或者是weakMap来判断循环引用
const deeo=(obj,cache=new WeakMap()){
    if(cache.get(obj)){
        return cache.get(obj)  //防止循环引用
    }
}
//消息通道
