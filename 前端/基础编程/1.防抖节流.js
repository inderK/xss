// 定义：  对于短时间内连续触发的事件（上面的滚动事件），防抖的含义就是让某个时间期限（如上面的1000毫秒）内，事件处理函数只执行一次。
// 实际运用：按钮频繁点击，页面resize。
const debounce = (callBack, time) => {

    let timer = null
    const _debounce = (...params) => {

        if (!!timer) {
            clearTimeout(timer)
        } else {
            timer = setTimeout(() => {
                console.log(params, "debounce")
                callBack.apply(this, params)
                timer = null
            }, time)
        }
    }
    return _debounce
}
debounce((pa) => { console.log(1, pa) })(2)



const throttle = (callBack, delay) => {
    let timer = null
    const _throttle = (...params) => {
        if (timer) {
            return
        } else {
            timer = setTimeout(() => {
                console.log(params, "throttle")

                callBack.apply(this, params)
                timer = null
            }, delay)
        }
    }
    return _throttle
}
throttle((pa) => { console.log(1, pa) })(2)