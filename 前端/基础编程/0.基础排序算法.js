//冒泡排序

const list = [5, 7, 8, 9, 4, 12, 5, 4, 1, 2, 365, 4,]

const bubbleSort = (list) => {
    let len = list.length
    for (let i = 0; i < len - 1; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (list[j] > list[j + 1]) {
                [list[j], list[j + 1]] = [list[j + 1], list[j]]
            }
        }
    }
    return list
}
console.log(bubbleSort(list))
/**
 * 下面是另外一种写法
 */

//sort 排序
const arr = [3, 2, 4, 1, 5]
arr.sort((a, b) => a - b)
console.log(arr)
//对字母进行排序
const arrA = ['a', 'c', 'h', 'b',]
arrA.sort()
console.log(arrA)


//冒泡排序
function bubbleSort(arr = []) {
    let len = arr.length
    for (let i = 0; i < len - 1; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                let num = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = num
            }
        }
    }
    return arr
}

console.log(bubbleSort([2, 56, 4, 1, 0, 3, 5, 8, 4]))
