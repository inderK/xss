let url = 'https://bimcloud.gcnao.cn/?token=eyJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50SWQiOiJjZDVjYjk2OWNiNmI0Y2M2OTNmNzUwMWY1YzIyOTI3MCIsImVtYWlsIjoiMzIxNDU2MzMzOEBxcS5jb20iLCJuYW1lIjoi6LC36aKWIiwidXNlcm5hbWUiOiJ6bGMxMjM0IiwicGhvbmVOdW1iZXIiOiIxODg1NTY2OTk4OCIsImFjY291bnRUeXBlIjoiUEVSU09OQUwiLCJ1c2VySWQiOiJhM2FhZGJiYmEzZjE0YzgxYTFiZTZkOGI3YWM1NzVlYiIsImNvbXBhbnlJZCI6IjU3NDM4ZDY0NzlmNTRlM2RiNTNjYTBhZmM2ZjFjOTZlIiwiY29tcGFueU5hbWUiOiLmt7HlnLPpq5jpgJ_lt6XnqIvpob7pl67mnInpmZDlhazlj7giLCJqdGktdXVpZCI6Imp0aS00NTM1MWVhNy0xY2ZhLTQ0OWUtOWU2Ny0xYzE1MTE0YzU3MzkiLCJleHAiOjE2NTA1OTM1ODF9.c1Xl9t3pvFMXIoOyxwl6rru0PKFG7zXDwLPPpR8J3Vs&appId=1db6fa6c13824e0690eeb9f9c46960d4&tenantId=2570328164164526b1917920dbc804b5&title=%E6%B7%B1%E6%B1%95%E9%A1%B9%E7%9B%AE&userId=a3aadbbba3f14c81a1be6d8b7ac575eb&url=https://dd-demo.gcnao.cn&urlGoback=https://dd-demo.gcnao.cn&goBack=https://dd-demo.gcnao.cn/desk'

/**
 * 方法一：使用新对象
 */
const urlSearParams = new URLSearchParams(url)
const params = Object.fromEntries(urlSearParams.entries())
// urlSearParams在控制台是打印不出来的，在编译器可以
// urlSearParams.entries()在控制台是打印不出来的，在编译器可以
// Object.entries(？) 是将对象转成一个自身可枚举属性的键值对数组
// urlSearParams.entries() 也是将自身转化成可枚举属性的键值对数组

// 使用 Object.fromEntries可以将键-值对嵌套数组转成对象
const nestedArray = [['key 1', ['key 1', 'value 1']], ['key 2', 'value 2']]
console.log(Object.fromEntries(nestedArray))
// 使用 Object.fromEntries 将 Map 转成对象 : 映射对象是键/值对的集合，其中键和值都可以是任意 ECMAScript 语言值。
const map = new Map()
map.set('key 1', 'value 1')
console.log(map, Object.fromEntries(map))

// urlSearParams本身就是类Map对象，所以可以直接使用Object.fromEntries(?)方法
console.log(JSON.stringify(params) == JSON.stringify(Object.fromEntries(urlSearParams)))
console.log(params)//可以使用属性值直接取到url的参数
// console.log(Object.entries(urlSearParams),)//没有效果，很好，因为它不是对象...叼  []


/**
 * 方法二：split方法
 */
function getParams(url = "") {
    const res = {}
    if (url.includes("?")) {
        const str = url.split("?")[1]       //?是唯一的
        const arr = str.split("&")          //&也是唯一的
        arr.forEach(item => {
            const key = item.split("=")[0]  //=也是唯一的
            const val = item.split("=")[1]
            // console.log(encodeURIComponent(encodeURIComponent(val),),encodeURIComponent(val),val)  //编码会改变的值，比如特殊符号/和汉字，再编码一次也会改变
            // console.log( decodeURIComponent(decodeURIComponent(val)))   //解码一直解码下去，就会返璞归真，不会改变
            res[key] = decodeURIComponent(val)  //解码，因为获取到的url是经过编码的，拿到作为值的话要经过解码才能原封不动的保存
        })
    }
    return res
}
console.log(getParams(url))


function getQueryVariable(variable) {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return (false);
}

/**
 * 方法三：正则写法
 */
function getUrlParam(url = "", name) {
    //字符串开头或者&开头 
    reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = url.match(reg);
    if (r != null) return decodeURIComponent(r[2]);     //如果没有获取到就是null,第一个是获取到的整个目标，第二个开始是& 第三个是目标，第四个是&
    return null;
}
console.log(getUrlParam(url, "title"))

