// 什么是声明式编程
// 声明式编程是一种编程范式，它关注的是你要做什么，而不是如何做。它表达逻辑而不显式地定义步骤。
// 这意味着我们需要根据逻辑的计算来声明要显示的组件。它没有描述控制流步骤。
// 声明式编程的例子有HTML、SQL等
// HTML file
// HTML
<div>
<p>Declarative Programming</p>
</div>
// SQL file
select * from studens where firstName = 'declarative';

// 声明式编程vs命令式编程
// 声明式编程的编写方式描述了应该做什么，而命令式编程描述了如何做。
// 在声明式编程中，让编译器决定如何做事情。
// 声明性程序很容易推理，因为代码本身描述了它在做什么。
// 下面是一个例子，数组中的每个元素都乘以2，我们使用声明式map函数，让编译器来完成其余的工作，而使用命令式，需要编写所有的流程步骤。
const numbers = [1,2,3,4,5];
// 声明式
const doubleWithDec = numbers.map(number => number * 2);
console.log(doubleWithDec)
// 命令式
const doubleWithlmp = [];
for(let i=0;i<numbers.length;i++){
const numberdouble = numbers[i] * 2;
doubleWithlmp.push(numberdouble)
}
console.log(doubleWithlmp)
