// 这个文件是node.js里面运行的，只能使用commonJS模块

const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: "./src/index.js",

    output: {
        filename: "bundle.js",
        // path:"./dist"  // 必须使用绝对路径
        path: path.resolve(__dirname, "./dist")
    },

    mode: "none",
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss', '.json'],
        alias: {
            //配置后可以直接用@来表示这个位置,直接应用目录下得文件,是绝对路径
            '@': path.join(__dirname, './src')
        }
    },
    module: {
        rules: [
            {
                //自動編譯 JSX 或 JS 檔 （require 可載入 JSX 了）
                test: /\.(js|jsx)$/,
                use: { loader: 'babel-loader' },
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        //https://github.com/jantimon/html-webpack-plugin
        //This will generate a file dist/index.html and autp-import index_bundle.js
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html'
        })
    ]
}