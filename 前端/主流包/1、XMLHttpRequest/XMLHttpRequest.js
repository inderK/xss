// window.onload = (e) => {
var xhr;
function loadXMLDoc(url) {

    // XMLHttpRequest 对象用于在后台与服务器交换数据。
    //任何 W3C 推荐标准均未规定 XMLHttpRequest 对象。

    if (window.XMLHttpRequest) {// code for IE7, Firefox, Opera, etc.
        xhr = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {// 老版本的 Internet Explorer （IE5 和 IE6）使用 ActiveX 对象：
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.open("get", url, true)//启动一个请求，未发送
    // True 表示脚本会在 send() 方法之后继续执行，而不等待来自服务器的响应。
    xhr.onreadystatechange = function () {
        switch (xhr.readyState) {
            case 0: alert("未初始化，及还未调用open方法");
                break;
            case 1: alert("启动，未调用send方法");
                break;
            case 2: alert("发送，未收到响应");
                break;
            case 3: alert("接受，取得部分数据");
                break;
            case 4: if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                alert(xhr.responceText);
            };
                break;
            default: alert("are you kidding?");
        }
    };
    // 可以在这个函数写对返回数据的处理
    // xhr.onreadystatechange = state_Change
    xhr.onerror = function () { alert("我出错啦") };
    xhr.send(null);//发送请求. 如果该请求是异步模式(默认),该方法会立刻返回. 相反,如果请求是同步模式,则直到请求的响应完全接受以后,该方法才会返回
}

function state_Change(xhr) {
    document.getElementById('A1').innerHTML = "xhr.status";
    document.getElementById('A2').innerHTML = "xhr.statusText";
    document.getElementById('A3').innerHTML = "xhr.responseText";
    if (xhr.readyState == 4) {// 4 = "loaded"
        if (xhr.status == 200) {// 200 = "OK"
            document.getElementById('A1').innerHTML = xhr.status;
            document.getElementById('A2').innerHTML = xhr.statusText;
            document.getElementById('A3').innerHTML = xhr.responseText;
        }
        else {
            alert("Problem retrieving XML data:" + xhr.statusText);
        }
    }
}