// 同步与异步的区别
// 　　同步提交：当用户发送请求时，当前页面不可以使用，服务器响应页面到客户端，响应完成，用户才可以使用页面。
// 　　异步提交：当用户发送请求时，当前页面还可以继续使用，当异步请求的数据响应给页面，页面把数据显示出来 。
// 1、要完整实现一个AJAX异步调用和局部刷新,通常需要以下几个步骤:
// 2、创建XMLHttpRequest对象,即创建一个异步调用对象.
// 3、设置回调函数,创建一个新的HTTP请求,并指定该HTTP请求的方法、URL及验证信息.设置响应HTTP请求状态变化的函数.
// 4、使用 open 方法与服务器建立连接,发送HTTP请求.
// 5、获取异步调用返回的数据,使用JavaScript和DOM实现局部刷新.
ajaxHttpRequestFunc = (e) => {
    var xmlHttpRequest; //定义一个变量,用于存放XMLHttpRequest对象
    createXMLHttpRequest(); //调用创建对象的方法
    //创建XMLHttpRequest对象的方法
    function createXMLHttpRequest() {
        if (window.ActiveXObject) {//判断是否是IE浏览器
            xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");//创建IE的XMLHttpRequest对象
        } else if (window.XMLHttpRequest) {//判断是否是Netscape等其他支持XMLHttpRequest组件的浏览器
            xmlHttpRequest = new XMLHttpRequest();//创建其他浏览器上的XMLHttpRequest对象
        }
    }
    // 创建http请求
    // xmlHttpRequest.open(method, URL, flag, name, password);
    // 　　method：该参数用于指定HTTP的请求方法，一共有get、post、head、put、delete五种方法，常用的方法为get和post。
    // 　　URL：该参数用于指定HTTP请求的URL地址，可以是绝对URL，也可以是相对URL。
    // 　　flag：该参数为可选，参数值为布尔型。该参数用于指定是否使用异步方式。true表示异步、false表示同步，默认为true。
    // 　　name：该参数为可选参数，用于输入用户名。如果服务器需要验证，则必须使用该参数。
    // 　　password：该参数为可选，用于输入密码。若服务器需要验证，则必须使用该参数。
    // 有下面两种
    // xmlHttpRequest.open("get", "http://www.aspxfans.com/BookSupport/JavaScript/ajax.htm", true);//预想情况是超时
    xmlHttpRequest.open("GET", "https://www.runoob.com/try/ajax/ajax_info.txt", true);//预想情况是跨域
    // xmlHttpRequest.open("get", "ajax.htm", true);
    // 创建完HTTP请求之后，应该就可以将HTTP请求发送给Web服务器了。然而，发送HTTP请求的目的是为了接收从服务器中返回的数据。
    // 从创建XMLHttpRequest对象开始，到发送数据、接收数据、XMLHttpRequest对象一共会经历以下5中状态。
    // 1、未初始化状态。在创建完XMLHttpRequest对象时，该对象处于未初始化状态，此时XMLHttpRequest对象的readyState属性值为0。
    // 2、初始化状态。在创建完XMLHttpRequest对象后使用open()方法创建了HTTP请求时，该对象处于初始化状态。此时XMLHttpRequest对象的readyState属性值为1。
    // 3、发送数据状态。在初始化XMLHttpRequest对象后，使用send()方法发送数据时，该对象处于发送数据状态，此时XMLHttpRequest对象的readyState属性值为2。
    // 4、接收数据状态。Web服务器接收完数据并进行处理完毕之后，向客户端传送返回的结果。此时，XMLHttpRequest对象处于接收数据状态，XMLHttpRequest对象的readyState属性值为3。
    // 5、完成状态。XMLHttpRequest对象接收数据完毕后，进入完成状态，此时XMLHttpRequest对象的readyState属性值为4。此时接收完毕后的数据存入在客户端计算机的内存中，可以使用responseText属性或responseXml属性来获取数据。
    // 只有在XMLHttpRequest对象完成了以上5个步骤之后，才可以获取从服务器端返回的数据。因此，如果要获得从服务器端返回的数据，就必须要先判断XMLHttpRequest对象的状态。
    //设置当XMLHttpRequest对象状态改变时调用的函数，注意函数名后面不要添加小括号
    xmlHttpRequest.onreadystatechange = getData;
    //定义函数
    function getData() {
        //判断XMLHttpRequest对象的readyState属性值是否为4，如果为4表示异步调用完成
        if (xmlHttpRequest.readyState == 4) {
            //设置获取数据的语句
        }
    }
    // 发送请求
    // let data = "name=myName&value=myValue"
    // xmlHttpRequest.send(data);//仅用于 POST 请求
    xmlHttpRequest.send(null);
    // 可能出现跨域
    // 这是因为代码中设置请求的是菜鸟驿站服务端的文件，所以出现跨域导致未正常获取到服务端返回的数据。
}