Axios 是一个基于 promise 的 HTTP 库，可以用在浏览器和 node.js 中。
axios 只是封装了 ajax 而已
两种方法
原生：
直接用 cdn，比如 index.html
依赖
npm install axios

axios 有那些特性？
　　 1、在浏览器中创建 XMLHttpRequests
　　 2、在 node.js 则创建 http 请求
　　 3、支持 Promise API
　　 4、支持拦截请求和响应
　　 5、转换请求和响应数据
　　 6、取消请求
　　 7、自动转换成 JSON 数据格式
　　 8、客户端支持防御 XSRF

传统 Ajax 指的是 XMLHttpRequest（XHR），axios 和 jQuer ajax 都是对 Ajax 的封装
