import axios from "axios"

// 为给定 ID 的 user 创建请求
axios.get('/user?ID=12345')
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log("error", error);
  });

// 可选地，上面的请求可以这样做
axios.get('/user', {
  params: {
    ID: 12345
  }
})
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });

// post请求
axios.post('/getMainInfo', {
  id: 123
})
  .then((res) => {
    console.log(res)
  })
  .catch((err) => {
    console.log(err)
  })

// const axios = require('axios');
// async function getUser() {
//   try {
//     const response = await axios.get('/user?ID=12345');
//     console.log(response);
//   } catch (error) {
//     console.error(error);
//   }
// }

// const axios = require('axios');
// function getUserAccount() {
//   return axios.get('/user/12345');
// }

// function getUserPermissions() {
//   return axios.get('/user/12345/permissions');
// }

// Promise.all([getUserAccount(), getUserPermissions()])
//   .then(function (results) {
//     const acct = results[0];
//     const perm = results[1];
//   });
