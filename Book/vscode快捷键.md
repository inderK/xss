Shift+Alt+Up：向上复制当前行
Shift+Alt+Down：向下复制当前行
Shift+Alt+Right：扩大选中范围
Shift+Alt+Left：缩小选中范围
Ctrl+U：回退到上一个光标处
Ctrl+Home：光标移动到文件开头（左上）
Ctrl+End：光标移动到文件结尾（右下）
Shift+Home：选择从光标到行首的内容
Shift+End：选择从光标到行尾的内容
F12：转到定义处
Alt+F12：查看定义处缩略图
Shift+Alt+Up：向上复制当前行
Shift+Alt+Down：向下复制当前行
Alt+Z：切换内容是否自动换行（底部显示/隐藏滚动条
Ctrl+Shift+L：选中所有匹配项（秀儿）
Ctrl+\：新建窗口显示代码（相当于复制当前代码到一个新的窗口；同一引用，修改一个文件，其他相同文件会一起改变）
Ctrl+B：显示/隐藏侧边栏
Ctrl +/- ：放大/缩小编辑器窗口
F11：全屏显示
Ctrl + Shift + M   打开错误tab页